﻿using PandB.Variables;
using UnityEngine;

namespace PandB.Example
{
	/// <summary>
	/// Place the game object at the position the player touched the screen.
	/// </summary>
	[RequireComponent(typeof(RectTransform))]
	public sealed class FingerFollower : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private Vector3Reference _fingerPosition = null;

		private RectTransform _rectTransform;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_rectTransform = GetComponent<RectTransform>();
			gameObject.SetActive(false);
		}

		private void OnEnable()
		{
			_rectTransform.anchoredPosition = _fingerPosition.Value;
		}

		private void LateUpdate()
		{
			_rectTransform.anchoredPosition = _fingerPosition.Value;
		}
		#endregion
	}
}