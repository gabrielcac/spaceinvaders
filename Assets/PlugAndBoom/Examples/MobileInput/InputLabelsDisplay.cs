﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace PandB.Example
{
	public sealed class InputLabelsDisplay : MonoBehaviour
	{
		#region private attributes
		[Header("Configuration")]
		[SerializeField]
		private Color _highlightColor = Color.red;
		[SerializeField]
		private Color _fadedColor = new Color(.5f, .5f, .5f, 0);
		[SerializeField]
		private float _fadeOutTime = 1.5f;
		[SerializeField]
		private float _fadeOutDelay = .5f;
		[SerializeField]
		private Ease _fadeOutEase = Ease.Linear;
		[Header("Components")]
		[SerializeField]
		private TextMeshProUGUI[] _labels = null;

		private Tween _fadeOutTween;
		#endregion

		#region MonoBehaviour implementation
		private void Start()
		{
			for (int i = _labels.Length - 1; i >= 0; i--)
			{
				_labels[i].gameObject.SetActive(false);
			}
		}
		#endregion

		#region public functions
		public void Show(TextMeshProUGUI label)
		{
			if (_fadeOutTween != null)
			{
				_fadeOutTween.Kill(true);
			}

			HideAllBut(label);

			label.color = _highlightColor;
			_fadeOutTween = DOTween.To(() => label.color, x => label.color = x, _fadedColor, _fadeOutTime).
				SetDelay(_fadeOutDelay).SetEase(_fadeOutEase).
				OnComplete(() => { label.gameObject.SetActive(false); _fadeOutTween = null; });
		}
		#endregion

		#region private functions
		private void HideAllBut(TextMeshProUGUI label)
		{
			for(int i = _labels.Length - 1; i >= 0; i-- )
			{
				if(_labels[i] != label)
				{
					_labels[i].gameObject.SetActive(false);
				}
				else
				{
					_labels[i].gameObject.SetActive(true);
				}
			}
		}
		#endregion
	}
}