﻿using PandB.I18N;
using UnityEngine;

namespace PandB.Examples
{
	public class UpdateTextParameter : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private I18NText _i18nText = null;
		#endregion

		#region public functions
		public void UpdateParameter(string value)
		{
			_i18nText.Parameters = new string[] { value };
		}
		#endregion
	}
}