﻿using TMPro;
using UnityEngine;

namespace PandB.Example
{
	/// <summary>
	/// Counts the number of elements in a set and updates the attached <see cref="TMP_Text"/> component.
	/// </summary>
	[RequireComponent(typeof(TMP_Text))]
	public class SetMembersCounter : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private RunTimeSet _set = null;

		private TMP_Text _text;
		#endregion

		#region private properties
		private TMP_Text Text
		{
			get
			{
				if (_text == null)
				{
					_text = GetComponent<TMP_Text>();
				}
				return _text;
			}
		}
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_text = GetComponent<TMP_Text>();
		}

		private void Start()
		{
			UpdateDisplay();
		}
		#endregion

		#region public function
		/// <summary>
		/// Updates the message with the number of enemies on screen.
		/// </summary>
		public void UpdateDisplay()
		{
			Text.text = "There are " + _set.Count + " objects in the set.";
		}
		#endregion
	}
}