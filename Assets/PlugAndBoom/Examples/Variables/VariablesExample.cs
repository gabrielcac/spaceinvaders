﻿using PandB.Variables;
using UnityEngine;

namespace PandB.Examples
{
	public class VariablesExample : MonoBehaviour
	{
		#region public attributes
		public FloatReference floatVariable = new FloatReference(5f);
		public IntReference intVariable;
		public BoolReference boolVariable;
		#endregion

		#region MonoBehaviour implementation
		private void Start()
		{
			Debug.Log("Float = " + floatVariable.Value);
			Debug.Log("Int = " + intVariable.Value);
			Debug.Log("Bool = " + boolVariable.Value);
		}
		#endregion
	}
}