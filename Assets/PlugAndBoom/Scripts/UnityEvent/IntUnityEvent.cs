﻿using System;
using UnityEngine.Events;

namespace PandB.UnityEvents
{
	/// <summary>
	/// An extension of <see cref="UnityEvent"/> that takes an int as a parameter.
	/// </summary>
	[Serializable]
	public sealed class IntUnityEvent : UnityEvent<int>
	{
	}
}