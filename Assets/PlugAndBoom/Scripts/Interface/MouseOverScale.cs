﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

namespace PandB.Interface
{
	/// <summary>
	/// Changes the scale of an object if the cursor moves over it.
	/// Restore the original scale of the object when the cursor leaves it.
	/// If it is attached to <see cref="UnityEngine.UI.Button"/>, restore the original scale when the button
	/// is clicked or deactivated. 
	/// </summary>
	public class MouseOverScale : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
	{
		#region public attributes
		[Header("Mouse Over Animation")]
		public Vector3 scale = new Vector3(1.1f, 1.1f, 1.1f);
		public float scaleUpTime = .25f;
		public Ease scaleUpEase = Ease.Linear;
		public float scaleDownTime = .25f;
		public Ease scaleDownEase = Ease.Linear;
		#endregion

		#region private attributes
		private Vector3 _originalScale;
		private UnityEngine.UI.Button _button;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_originalScale = transform.localScale;
			_button = GetComponent<UnityEngine.UI.Button>();
		}

		private void OnDisable()
		{
			DOTween.Kill(GetInstanceID().ToString() + "_MouseOverScale");
			transform.localScale = _originalScale;
		}
		#endregion

		#region IPointerEnterHandler implementation
		public void OnPointerEnter(PointerEventData eventData)
		{
			if (_button == null || _button.interactable)
			{
				DOTween.Kill(GetInstanceID().ToString() + "_MouseOverScale");
				DOTween.To(() => transform.localScale, x => transform.localScale = x, scale, scaleUpTime).
						SetEase(scaleUpEase).SetId(GetInstanceID().ToString() + "_MouseOverScale");
			}
		}
		#endregion

		#region IPointerExitHandler implementation
		public void OnPointerExit(PointerEventData eventData)
		{
			DOTween.Kill(GetInstanceID().ToString() + "_MouseOverScale");
			DOTween.To(() => transform.localScale, x => transform.localScale = x, _originalScale, scaleDownTime).
					SetEase(scaleDownEase).SetId(GetInstanceID().ToString() + "_MouseOverScale");
		}
		#endregion

		#region IPointerDownHandler implementation
		public void OnPointerDown(PointerEventData eventData)
		{
			if (_button != null)
			{
				DOTween.Kill(GetInstanceID().ToString() + "_MouseOverScale");
				transform.localScale = _originalScale;
			}
		}
		#endregion
	}
}