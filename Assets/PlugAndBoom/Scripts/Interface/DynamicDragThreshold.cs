﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace PandB.Interface
{
	/// <summary>
	/// Automatically configures the <see cref="EventSystem.pixelDragThreshold"/> based on the screen DPI.
	/// </summary>
	[RequireComponent(typeof(EventSystem))]
	public class DynamicDragThreshold : MonoBehaviour
	{
		#region public functions
		public float referenceDPI = 100;
		public float referencePixelDrag = 8f;
		#endregion

		#region MonoBehaviour implementation
		void Awake()
		{
			UpdatePixelDrag(Screen.dpi);
		}
		#endregion

		#region private functions
		/// <summary>
		/// Updates the pixel drag threshold of the EventSystem based on the screen dpi.
		/// </summary>
		/// <param name="screenDpi"></param>
		private void UpdatePixelDrag(float screenDpi)
		{
			GetComponent<EventSystem>().pixelDragThreshold = Mathf.RoundToInt(screenDpi / referenceDPI * referencePixelDrag);
		}
		#endregion
	}
}