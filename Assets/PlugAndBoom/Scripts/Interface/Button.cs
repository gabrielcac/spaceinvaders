using DG.Tweening;
using UnityEngine;

namespace PandB.Interface
{
	/// <summary>
	/// Generic button behaviour used in all the game buttons.
	/// </summary>
	[RequireComponent(typeof(UnityEngine.UI.Button))]
	public abstract class Button : MonoBehaviour, IPausable
	{
		#region public attributes
		public float onClickPunchScale = .2f;
		public float punchAnimationTime = .1f;
		#endregion

		#region MonoBehaviour implementation
		protected virtual void Awake()
		{
			GetComponent<UnityEngine.UI.Button>().onClick.AddListener(OnClick);
		}

		protected virtual void OnDestroy()
		{
			DOTween.Kill(GetInstanceID());
		}
		#endregion

		#region IPausable implementation
		public virtual void Pause()
		{
			DOTween.Pause(GetInstanceID());
		}

		public virtual void Resume()
		{
			DOTween.Play(GetInstanceID());
		}
		#endregion

		#region public functions
		/// <summary>
		/// Function called by the UI when the button is clicked. This function will play the button click animation and
		/// then call the ClickFunction, where the button functionality should be implemented.
		/// </summary>
		public virtual void OnClick()
		{
			DOTween.Kill(GetInstanceID());
			transform.localScale = Vector3.one;
			DOTween.Punch(() => transform.localScale, x => transform.localScale = x, onClickPunchScale * Vector3.one, punchAnimationTime)
				.OnComplete(ClickFunction).SetId(GetInstanceID());
		}
		#endregion

		#region protected functions
		/// <summary>
		/// <para>Function to be overwritten by other classes implementing this.</para>
		/// <para>This function is called right after the button click animation ends.</para>
		/// </summary>
		protected abstract void ClickFunction();
		#endregion
	}
}