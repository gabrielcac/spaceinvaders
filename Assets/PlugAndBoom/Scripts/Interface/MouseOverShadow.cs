﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

namespace PandB.Interface
{
	/// <summary>
	/// Changes the shadow of an object if the cursor moves over it.
	/// Restore the original shadow size of the object when the cursor leaves it.
	/// If it is attached to <see cref="UnityEngine.UI.Button"/>, restore the original shadow when the button
	/// is clicked or deactivated. 
	/// </summary>
	public class MouseOverShadow : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
	{
		#region public attributes
		[Header("Components")]
		public Shadow shadow;
		[Header("Mouse Over Animation")]
		public Vector2 shadowDistance;
		public float mouseInTime = .25f;
		public Ease mouseInEase = Ease.Linear;
		public float mouseOutTime = .25f;
		public Ease mouseOutEase = Ease.Linear;
		#endregion

		#region private attributes
		private Vector2 _originalShadowDistance;
		private UnityEngine.UI.Button _button;
		#endregion

		#region MonoBehaviour implementation
		private void Reset()
		{
			shadow = GetComponentInChildren<Shadow>();
		}

		private void Awake()
		{
			_button = GetComponent<UnityEngine.UI.Button>();

			_originalShadowDistance = shadow.effectDistance;
		}

		private void OnDisable()
		{
			DOTween.Kill(GetInstanceID().ToString() + "_MouseOverShadow");
			shadow.effectDistance = _originalShadowDistance;
		}
		#endregion

		#region IPointerEnterHandler implementation
		public void OnPointerEnter(PointerEventData eventData)
		{
			if (_button == null || _button.interactable)
			{
				DOTween.Kill(GetInstanceID().ToString() + "_MouseOverShadow");
				DOTween.To(() => shadow.effectDistance, x => shadow.effectDistance = x, shadowDistance, mouseInTime).
						SetEase(mouseInEase).SetId(GetInstanceID().ToString() + "_MouseOverShadow");
			}
		}
		#endregion

		#region IPointerExitHandler implementation
		public void OnPointerExit(PointerEventData eventData)
		{
			DOTween.Kill(GetInstanceID().ToString() + "_MouseOverShadow");
			DOTween.To(() => shadow.effectDistance, x => shadow.effectDistance = x, _originalShadowDistance, mouseOutTime).
					SetEase(mouseOutEase).SetId(GetInstanceID().ToString() + "_MouseOverShadow");
		}
		#endregion

		#region IPointerDownHandler implementation
		public void OnPointerDown(PointerEventData eventData)
		{
			if (_button != null)
			{
				DOTween.Kill(GetInstanceID().ToString() + "_MouseOverShadow");
				shadow.effectDistance = _originalShadowDistance;
			}
		}
		#endregion
	}
}