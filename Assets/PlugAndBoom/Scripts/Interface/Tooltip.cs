﻿using PandB.Variables;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PandB.Interface
{
	/// <summary>
	/// If the player leaves the cursor over this component, as tooltip will show up with the 
	/// specified text.
	/// </summary>
	public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
	{
		#region private attributes
		[SerializeField]
		private StringReference _tooltipText = null;
		#endregion

		#region private attributes
		private TooltipDisplay _display;
		#endregion

		#region public properties
		/// <summary>
		/// Gets the tooltip text.
		/// </summary>
		public string Text
		{
			get
			{
				return _tooltipText.Value;
			}
		}
		#endregion

		#region MonoBehaviour implementation
		private void OnDisable()
		{
			if(_display != null)
			{
				_display.Hide();
			}
		}
		#endregion

		#region IPointerEnterHandler implementation
		public void OnPointerEnter(PointerEventData eventData)
		{
			_display = TooltipController.ShowTooltip(this, eventData.position);
		}
		#endregion

		#region IPointerExitHandler implementation
		public void OnPointerExit(PointerEventData eventData)
		{
			if (_display != null)
			{
				_display.Hide();
			}
		}
		#endregion

		#region IPointerDownHandler implementation
		public void OnPointerDown(PointerEventData eventData)
		{
			if (_display != null)
			{
				_display.Hide();
			}
		}
		#endregion
	}
}