﻿using System.Collections.Generic;
using UnityEngine;

namespace PandB.Interface
{
	/// <summary>
	/// Places tooltips in the screen.
	/// </summary>
	public class TooltipController : MonoBehaviour
	{
		#region private static attributes
		private static TooltipController _instance;
		#endregion

		#region private attributes
		[SerializeField]
		private TooltipDisplay _tooltipPrefab = null;

		private Stack<TooltipDisplay> _displayPool = new Stack<TooltipDisplay>();
		private Camera _canvasCamera;
		#endregion

		#region public static properties
		/// <summary>
		/// Gets the single instance of this class.
		/// </summary>
		public static TooltipController Instance
		{
			get
			{
				return _instance;
			}
		}
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			if(_instance != null)
			{
				Destroy(gameObject);
				return;
			}
			_instance = this;

			Canvas toolTipCanvas = GetComponent<Canvas>();
			if (toolTipCanvas.renderMode != RenderMode.ScreenSpaceOverlay)
			{
				_canvasCamera = toolTipCanvas.worldCamera;
			}
		}

		private void OnEnable()
		{
			TooltipDisplay.OnTooltipHidden += OnTooltipHidden;
		}

		private void OnDisable()
		{
			TooltipDisplay.OnTooltipHidden -= OnTooltipHidden;
		}

		private void OnDestroy()
		{
			if(_instance == this)
			{
				_instance = null;
			}
		}
		#endregion

		#region public static functions
		/// <summary>
		/// Shows a tooltip as specified by the <see cref="Tooltip"/> object.
		/// </summary>
		/// <param name="tooltip"></param>
		/// <returns></returns>
		public static TooltipDisplay ShowTooltip(Tooltip tooltip, Vector2 pointerScreenPosition)
		{
			if (Instance == null)
			{
				return null;
			}

			TooltipDisplay display = Instance.GetAvailableDisplay();

			// Sets the Tooltip pivot
			RectTransform tooltipRectTransform = display.GetComponent<RectTransform>();
			if (tooltip.transform.position.x >= 0)
			{
				tooltipRectTransform.pivot = new Vector2(1f, .5f);
			}
			else
			{
				tooltipRectTransform.pivot = new Vector2(0, .5f);
			}

			// Move the tooltip to the correct position
			if (Instance._canvasCamera == null)
			{
				tooltipRectTransform.position = pointerScreenPosition;
			}
			else
			{
				RectTransformUtility.ScreenPointToLocalPointInRectangle(
					Instance.GetComponent<RectTransform>(),
					pointerScreenPosition, Instance._canvasCamera, out Vector2 outPosition);
				tooltipRectTransform.localPosition = outPosition;
			}

			display.Show(tooltip);

			return display;
		}
		#endregion

		#region private functions
		/// <summary>
		/// Gets an available disply object from the pool.
		/// </summary>
		/// <returns></returns>
		private TooltipDisplay GetAvailableDisplay()
		{
			if(_displayPool.Count > 0)
			{
				return _displayPool.Pop();
			}
			TooltipDisplay display = Instantiate(_tooltipPrefab, transform);
			return display;
		}

		/// <summary>
		/// Called as a Tooltip display is hidden, to add it to the pool.
		/// </summary>
		/// <param name="display"></param>
		private void OnTooltipHidden(TooltipDisplay display)
		{
			_displayPool.Push(display);
		}
		#endregion
	}
}