﻿using DG.Tweening;
using UnityEngine;
using TMPro;
using PandB.Variables;

namespace PandB.Interface
{
	/// <summary>
	/// Display a tooltip as the player moves the cursor over an UI element.
	/// </summary>
	public class TooltipDisplay : MonoBehaviour
	{
		#region public events
		public delegate void TooltipHidden(TooltipDisplay display);
		public static event TooltipHidden OnTooltipHidden;
		#endregion

		#region private attributes
		[Header("Configuration")]
		[SerializeField]
		private FloatReference _timeToShow = new FloatReference(1f);
		[SerializeField]
		private FloatReference _showAnimationTime = new FloatReference(.15f);
		[SerializeField]
		private Ease _scaleUpEase = Ease.Linear;
		[Header("Components")]
		[SerializeField]
		private TMP_Text _textField = null;
		#endregion

		#region MonoBehaviour implementation
		private void OnDisable()
		{
			DOTween.Kill(GetInstanceID());
		}
		#endregion

		#region public functions
		/// <summary>
		/// Start whowing the tooltip.
		/// </summary>
		public void Show(Tooltip tooltip)
		{
			_textField.text = tooltip.Text;

			transform.localScale = new Vector3(0, 1, 1);
			DOTween.Kill(GetInstanceID());
			DOTween.To(() => transform.localScale, x => transform.localScale = x, Vector3.one, _showAnimationTime.Value).
					SetEase(_scaleUpEase).SetDelay(_timeToShow.Value).SetId(GetInstanceID());
		}

		/// <summary>
		/// Hides the tooltip.
		/// </summary>
		public void Hide()
		{
			DOTween.Kill(GetInstanceID());
			transform.localScale = new Vector3(0, 1, 1);

			if(OnTooltipHidden != null)
			{
				OnTooltipHidden(this);
			}
		}
		#endregion
	}
}