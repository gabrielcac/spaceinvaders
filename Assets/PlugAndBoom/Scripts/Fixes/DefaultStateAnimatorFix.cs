﻿using System;
using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Fix a problem where the <see cref="Animator"/> would consider the default
	/// state of an object to be the state it was when the <see cref="Animator"/> 
	/// was enabled, and not the state of the prefab.
	/// </summary>
	[RequireComponent(typeof(Animator))]
	public sealed class DefaultStateAnimatorFix : MonoBehaviour
	{
		#region private attributes
		private Transform[] _transforms;
		private Behaviour[] _behaviour;
		#endregion

		#region private attributes
		private ObjectInfo[] _info;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_transforms = GetComponentsInChildren<Transform>(true);
			_behaviour = GetComponentsInChildren<MonoBehaviour>(true);

			_info = new ObjectInfo[_transforms.Length - 1 + _behaviour.Length];
			int infoIndex = 0;
			for(int i = 0, j = _transforms.Length; i < j; i++)
			{
				if(_transforms[i] != transform)
				{
					_info[infoIndex] = new TransformInfo(_transforms[i]);
					infoIndex++;
				}
			}
			for (int i = 0, j = _behaviour.Length; i < j; i++)
			{
				_info[infoIndex] = new ComponentInfo(_behaviour[i], _behaviour[i].GetType());
				infoIndex++;
			}
		}

		private void OnDisable()
		{
			for (int i = 0, j = _info.Length; i < j; i++)
			{
				_info[i].Restore();
			}
		}
		#endregion

		#region private classes
		/// <summary>
		/// Holds data on an object to be reset when the <see cref="GameObject"/>
		/// is disabled.
		/// </summary>
		private abstract class ObjectInfo
		{
			#region public functions
			/// <summary>
			/// Restore the object to its initial state.
			/// </summary>
			public abstract void Restore();
			#endregion
		}

		/// <summary>
		/// Stores information about a <see cref="Transform"/>.
		/// </summary>
		private class TransformInfo : ObjectInfo
		{
			#region private attributes
			private Transform _transform;
			private bool _enabled;
			private Vector3 _localPosition;
			private Quaternion _localRotation;
			private Vector3 _localScale;
			#endregion

			#region ObjectInfo implementation
			public override void Restore()
			{
				if (_transform.gameObject.activeSelf != _enabled)
				{
					_transform.gameObject.SetActive(_enabled);
				}
				_transform.localPosition = _localPosition;
				_transform.localRotation = _localRotation;
				_transform.localScale = _localScale;
			}
			#endregion

			#region public functions
			public TransformInfo(Transform transform)
			{
				_transform = transform;
				_enabled = _transform.gameObject.activeSelf;
				_localPosition = transform.localPosition;
				_localRotation = transform.localRotation;
				_localScale = transform.localScale;
			}
			#endregion
		}

		/// <summary>
		/// Holds serializaed data of a <see cref="Component"/>.
		/// </summary>
		private class ComponentInfo : ObjectInfo
		{
			#region private attributes
			private Type _type;
			private UnityEngine.Object _object;
			private string _json;
			#endregion

			#region ObjectInfo implementation
			public override void Restore()
			{
				JsonUtility.FromJsonOverwrite(_json, Convert.ChangeType(_object, _type));
			}
			#endregion

			#region public functions
			public ComponentInfo(UnityEngine.Object serializableObject, Type type)
			{
				_object = serializableObject;
				_type = type;

				_json = JsonUtility.ToJson(Convert.ChangeType(_object, _type));
			}
			#endregion
		}

		#endregion
	}
}