﻿using UnityEngine;
using UnityEngine.Events;

namespace PandB
{
	/// <summary>
	/// Listens to a <see cref="GameEvent"/> and call an UnityEvent.
	/// </summary>
	public class GameEventListener : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private GameEvent _gameEvent = null;
		[SerializeField]
		private UnityEvent _onEventRaised = null;
		#endregion

		#region MonoBehaviour implementation
		private void OnEnable()
		{
			_gameEvent.AddListener(this);
		}

		private void OnDisable()
		{
			_gameEvent.RemoveListener(this);
		}
		#endregion

		#region public functions
		/// <summary>
		/// Called by the <see cref="GameEvent"/> object when the event is raised.
		/// </summary>
		public void OnEventRaised()
		{
			_onEventRaised.Invoke();
		}
		#endregion
	}
}