﻿using System.Collections.Generic;
using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Events raised in the game.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.gameEventFileName, menuName = CreateMenus.gameEventMenu, 
		order = CreateMenus.gameEventOrder)]
	public class GameEvent : ScriptableObject
	{
		#region private attributes
#pragma warning disable CS0414
		[SerializeField]
		[Multiline]
		private string _description = null;
#pragma warning restore CS0414

		private List<GameEventListener> _actions = new List<GameEventListener>();
		#endregion

		#region public functions
		/// <summary>
		/// Raises the event to all listeners.
		/// </summary>
		public void Raise()
		{
			for(int i = _actions.Count-1; i >= 0; i--)
			{
				_actions[i].OnEventRaised();
			}
		}

		/// <summary>
		/// Adds a listener to the event.
		/// </summary>
		/// <param name="listener"></param>
		public void AddListener(GameEventListener listener)
		{
			_actions.Add(listener);
		}

		/// <summary>
		/// Removes an event listener.
		/// </summary>
		/// <param name="listener"></param>
		public void RemoveListener(GameEventListener listener)
		{
			_actions.Remove(listener);
		}
		#endregion
	}
}