using PandB.Variables;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PandB.I18N
{
	/// <summary>
	/// Sets the text of a Text component according to the language defined in the player preferences.
	/// </summary>
	public class I18NText : MonoBehaviour
	{
        #region private attributes
        [SerializeField]
        private I18NDictionary _dictionary = null;
		[SerializeField]
		private StringReference _i18n_key = null;
		[SerializeField]
		private string[] _parameters;

		private Text _uiText;
		private TextMesh _textMesh;
		private TMP_Text _textMeshPro;
		private bool _initialized = false;
		#endregion

		#region public properties
		/// <summary>
		/// The internationalized key.
		/// </summary>
		public string I18NKey
		{
			get
			{
				return _i18n_key.Value;
			}
			set
			{
				_i18n_key.Value = value;
				if (gameObject.activeSelf && _initialized)
				{
					RefreshText();
				}
			}
		}

		/// <summary>
		/// Parameters used to assemble the string.
		/// </summary>
		public string[] Parameters
		{
			get
			{
				return _parameters;
			}
			set
			{
				_parameters = value;
				if (gameObject.activeSelf && _initialized)
				{
					RefreshText();
				}
			}
		}
		#endregion

		#region MonoBehaviour functions
		private void Awake()
		{
			_uiText = GetComponent<Text>();
			_textMesh = GetComponent<TextMesh>();
			_textMeshPro = GetComponent<TMP_Text>();

			if (_uiText == null && _textMesh == null && _textMeshPro == null)
			{
				Debug.LogWarning("I18NText needs to be used with a Text, TextMesh or TMP_Text!");
			}

			_initialized = true;

            _dictionary.OnUpdateLabels += OnUpdateLabels;
		}

		private void OnEnable()
		{
			RefreshText();
		}

		private void OnDestroy()
		{
            _dictionary.OnUpdateLabels -= OnUpdateLabels;
		}
		#endregion

		#region private functions
		/// <summary>
		/// Called every time the language configuration is changed by the user, to update this object text.
		/// </summary>
		private void OnUpdateLabels()
		{
			if (gameObject.activeSelf)
			{
				RefreshText();
			}
		}

		/// <summary>
		/// Updates the Text component attached to the object with the correct text.
		/// </summary>
		private void RefreshText()
		{
			if (!string.IsNullOrEmpty(_i18n_key.Value))
			{
				string text = null;
				if (Parameters == null || Parameters.Length == 0)
				{
					text = _dictionary.I18N[_i18n_key.Value];
				}
				else
				{
					text = string.Format(_dictionary.I18N[_i18n_key.Value], Parameters);
				}

				if (_uiText != null)
				{
					_uiText.text = text;
				}
				else if (_textMesh != null)
				{
					_textMesh.text = text;
				}
				else
				{
					_textMeshPro.text = text;
				}
			}
			else
			{
				Debug.LogWarning("Missing I18N key on game object " + gameObject.name, gameObject);
			}
		}
		#endregion
	}
}