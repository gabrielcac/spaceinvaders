using UnityEngine;
using System.Collections.Generic;
using System.IO;
using PandB.Variables;

namespace PandB.I18N
{
	/// <summary>
	/// <para>This class will create a dictionary to internationalize the game.</para>
	/// <para>The name of the language files should be the same as the name of the languages in 
	/// http://docs.unity3d.com/ScriptReference/SystemLanguage.html </para>
	/// <para>This class uses the singleton pattern to avoid conflicting configurations. It also uses the observable pattern
	/// to warn the <see cref="I18NText"/>, <see cref="I18NSprite"/> and <see cref="I18NImage"/> classes about changes in the 
	/// language configuration.</para>
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.dictionaryFileName, menuName = CreateMenus.dictionaryMenu, order = CreateMenus.dictionaryOrder)]
    public class I18NDictionary : ScriptableObject, ISerializationCallbackReceiver
    {
        #region public events
        public delegate void UpdateLabels();
		public event UpdateLabels OnUpdateLabels;
        #endregion

        #region private attributes
		[Tooltip("List of files with the supported translations. The first file will be considered the default value.")]
		[SerializeField]
		private TextAsset[] _files = null;
        [Tooltip("Key used in the player prefs to store the language selected by the player.")]
        [SerializeField]
        private StringReference _playerPrefsKey = new StringReference("SelectedLanguage");
		[Tooltip("Event raised when the language is changed.")]
		[SerializeField]
		private GameEvent _onLanguageUpdated = null;

        private Dictionary<string, string> _i18nStrings;
		private string _selectedLanguage;
        private List<string> _supportedLanguages = null;
        #endregion

        #region public properties
        /// <summary>
        /// Gets the dictionary that maps a key to its exhibition value.
        /// </summary>
        /// <value>The lang.</value>
        public Dictionary<string,string> I18N
		{
			get
			{
                if(_i18nStrings == null)
                {
                    _i18nStrings = LoadI18NStrings();
                }
				return _i18nStrings;
			}
		}

		/// <summary>
		/// Gets or sets the dictionary language. Only sets the language if it is a value 
		/// from the <see cref="SupportedLanguages"/> list.
		/// </summary>
		/// <value>The locale.</value>
		public string Language
		{
			get
			{
                if (string.IsNullOrEmpty(_selectedLanguage))
                {
                    _selectedLanguage = PlayerPrefs.GetString(_playerPrefsKey.Value);

                    if (string.IsNullOrEmpty(_selectedLanguage))
                    {
                        _selectedLanguage = Application.systemLanguage.ToString();
                    }
                }
                return _selectedLanguage;
			}
			set
			{
				if(SupportedLanguages.Contains(value))
				{
					_selectedLanguage = value;
					_i18nStrings = LoadI18NStrings();
					
					PlayerPrefs.SetString(_playerPrefsKey.Value, _selectedLanguage);
					PlayerPrefs.Save();
					
					if(OnUpdateLabels != null)
					{
						OnUpdateLabels();
					}

					if(_onLanguageUpdated != null)
					{
						_onLanguageUpdated.Raise();
					}
				}
			}
        }

        /// <summary>
        /// Gets the default language to use when the configured language is not suppoerted.
        /// </summary>
        public string DefaultLanguage
        {
            get
            {
                return _files[0].name;
            }
        }

        /// <summary>
        /// Gets a list of all the supported languages.
        /// </summary>
        /// <value>The supported languages.</value>
        public List<string> SupportedLanguages
        {
            get
            {
                if (_supportedLanguages == null)
                {
                    LoadSupportedLanguages();
                }
                return _supportedLanguages;
            }
        }
        #endregion

        #region ISerializationCallbackReceiver implementation
        public void OnAfterDeserialize()
        {
            _i18nStrings = null;
			_selectedLanguage = null;
			_supportedLanguages = null;
        }

        public void OnBeforeSerialize()
        {
        }
		#endregion

		#region public functions
		/// <summary>
		/// Sets the selected language to be the next language in the <see cref="SupportedLanguages"/> list.
		/// </summary>
		public void NextLanguage()
        {
            int index = SupportedLanguages.BinarySearch(Language);
            index++;
            if (index == SupportedLanguages.Count)
            {
                index = 0;
            }
            Language = SupportedLanguages[index];
        }

		/// <summary>
		/// Sets the selected language to be the previous language in the <see cref="SupportedLanguages"/> list.
		/// </summary>
		public void PreviousLanguage()
        {
            int index = SupportedLanguages.BinarySearch(Language);
            index--;
            if (index == -1)
            {
                index = SupportedLanguages.Count - 1;
            }
            Language = SupportedLanguages[index];
        }
        #endregion

        #region private functions
		/// <summary>
		/// <para>Generates the dictionary with all the keys and their exhibition values.</para>
		/// <para>If the language configured is not supported, load the game in the default language.</para>
		/// </summary>
		/// <returns>The lang.</returns>
		private Dictionary<string, string> LoadI18NStrings()
        {
			Dictionary<string, string> dictionary = new Dictionary<string, string>();

			TextAsset textAsset = FindTextFile(Language);

			if(textAsset == null)
			{
                Language = DefaultLanguage;
				textAsset = FindTextFile(Language);
			}

			using (StringReader reader = new StringReader(textAsset.text))
			{
				string[] line_content = null;
				string line;
				while ((line = reader.ReadLine()) != null)
				{
					if (line.Length > 0)
					{
						line_content = line.Split(new char[] { '=' }, 2);
						dictionary.Add(line_content[0], line_content[1].Replace("\\n", "\n"));
					}
				}
				reader.Close();
			}

			return dictionary;
		}

		/// <summary>
		/// Loads the list of supported languages.
		/// </summary>
		private void LoadSupportedLanguages()
		{
			_supportedLanguages = new List<string>();
			foreach (TextAsset textAsset in _files)
			{
				_supportedLanguages.Add(textAsset.name);
			}
		}

		/// <summary>
		/// Finds the text file corresponding to the specified locale.
		/// </summary>
		/// <param name="locale"></param>
		/// <returns></returns>
		private TextAsset FindTextFile(string locale)
		{
			foreach (TextAsset textAsset in _files)
			{
				if(textAsset.name == locale)
				{
					return textAsset;
				}
			}
			return null;
		}
        #endregion
	}
}