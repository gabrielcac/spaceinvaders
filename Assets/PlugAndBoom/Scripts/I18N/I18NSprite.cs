using UnityEngine;

namespace PandB.I18N
{

	/// <summary>
	/// Updates the sprite according to player language defined by the user.
	/// </summary>
	public class I18NSprite : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private I18NDictionary _dictionary = null;
		[SerializeField]
		private string _path = null;
		[SerializeField]
		private string _prefix = null;
		#endregion

		#region MonoBehaviour implementation
		private void OnEnable()
		{
            _dictionary.OnUpdateLabels += OnUpdateLabels;
		}

		private void Start()
		{
			RefreshImage();
		}

		private void OnDisable()
		{
            _dictionary.OnUpdateLabels -= OnUpdateLabels;
		}
		#endregion

		#region public functions
		/// <summary>
		/// Called every time the language configuration is changed by the user, to update this object sprite.
		/// </summary>
		public void OnUpdateLabels()
		{
			RefreshImage();
		}
		#endregion

		#region private functions
		/// <summary>
		/// Refreshs the sprite of the Image component according to the language defined in the player preferences.
		/// </summary>
		private void RefreshImage()
		{
			Sprite sprite = Resources.Load<Sprite>(_path + "/" + _prefix + _dictionary.Language);
			if(sprite == null)
			{
				sprite = Resources.Load<Sprite>(_path + "/" + _prefix + _dictionary.DefaultLanguage);
			}
			transform.GetComponent<SpriteRenderer>().sprite = sprite;
		}
		#endregion
	}
}