﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using PandB.Variables;
using PandB.UnityEvents;

namespace PandB.Inputs
{
	/// <summary>
	/// Interprets input from a user on a mobile device.
	/// </summary>
	public sealed class MobileInputHandler : MonoBehaviour, IPausable
	{
		#region private variables
		[Header("Click Configuration")]
		[SerializeField]
		private FloatReference _maxClickTime = new FloatReference(.5f);

		[Header("Move Configuration")]
		[SerializeField]
		private FloatReference _moveMinDistance = new FloatReference(10f);
		[SerializeField]
		private FloatReference _minTimeToStationary = new FloatReference(.5f);

		[Header("Swipe Configuration")]
		[SerializeField]
		private FloatReference _swipeMaxTime = new FloatReference(.5f);
		[SerializeField]
		private FloatReference _swipeMinDistance = new FloatReference(20f);
		[SerializeField]
		private FloatReference _referenceDPI = new FloatReference(100f);

		[Header("General Configuration")]
		[SerializeField]
		private IntReference _simultaneousInputs = new IntReference(1);

		[Header("Scene References")]
		[SerializeField]
		private Canvas _hudCanvas = null;

		[Header("Exits")]
		[SerializeField]
		private IntUnityEvent _onScreenTouched = null;
		[SerializeField]
		private IntUnityEvent _onScreenUntouched = null;
		[SerializeField]
		private IntUnityEvent _onClick = null;
		[SerializeField]
		private IntUnityEvent _onHold = null;
		[SerializeField]
		private IntUnityEvent _onMove = null;
		[SerializeField]
		private IntUnityEvent _onSwipeUp = null;
		[SerializeField]
		private IntUnityEvent _onSwipeDown = null;
		[SerializeField]
		private IntUnityEvent _onSwipeLeft = null;
		[SerializeField]
		private IntUnityEvent _onSwipeRight = null;
		[SerializeField]
		private Vector3Reference[] _screenPositions = null;

		private Dictionary<int, TouchData> _touches;
		private bool _paused;
		private float _minSwipeDistance;
		private float _minMoveDistance;
		#endregion

		#region Monobehaviour Implementations
		private void Awake()
		{
			UpdatePixelDrag(Screen.dpi);
		}

		private void Start()
		{
			_touches = new Dictionary<int, TouchData>();
		}

		private void Update()
		{
			int analyzed = 0;
			foreach (Touch touch in Input.touches)
			{
				analyzed++;
				if (analyzed > _simultaneousInputs.Value)
				{
					break;
				}

				//For every touch in the Input.touches - array...
				ProcessTouch(touch);
			}

#if UNITY_EDITOR
			bool touched = false;
			Touch mouseTouch = new Touch();
			mouseTouch.fingerId = 0;
			mouseTouch.position = new Vector2(
					Mathf.Clamp(Input.mousePosition.x, 0, Screen.width),
					Mathf.Clamp(Input.mousePosition.y, 0, Screen.height));
			// All mouse input is considered a touch input with finger id 0
			if (Input.GetMouseButtonDown(0))
			{
				mouseTouch.phase = TouchPhase.Began;
				touched = true;
			}
			if (Input.GetMouseButton(0))
			{
				if(_touches.ContainsKey(0))
				{
					if (_touches[0].Position == mouseTouch.position)
					{
						mouseTouch.phase = TouchPhase.Stationary;
					}
					else
					{
						mouseTouch.phase = TouchPhase.Moved;
					}
					touched = true;
				}
			}
			if (Input.GetMouseButtonUp(0))
			{
				if (_touches.ContainsKey(0))
				{
					mouseTouch.phase = TouchPhase.Ended;
					touched = true;
				}
			}

			if (touched)
			{
				ProcessTouch(mouseTouch);
			}
			else
			{
				if (Input.GetKeyDown(KeyCode.W))
				{
					_onSwipeUp.Invoke(0);
				}
				else if (Input.GetKeyDown(KeyCode.S))
				{
					_onSwipeDown.Invoke(0);
				}
				else if (Input.GetKeyDown(KeyCode.A))
				{
					_onSwipeLeft.Invoke(0);
				}
				else if (Input.GetKeyDown(KeyCode.D))
				{
					_onSwipeRight.Invoke(0);
				}
			}
#endif
		}

		private void OnDisable()
		{
			int[] keys = new int[_touches.Count];
			_touches.Keys.CopyTo(keys, 0);
			for (int i = 0, j = keys.Length; i < j; i++)
			{
				Touch touch = new Touch();
				touch.fingerId = keys[i];
				touch.position = _touches[keys[i]].Position;
				touch.phase = TouchPhase.Ended;
				ProcessTouch(touch);
			}
		}
		#endregion

		#region IPausable implementation
		public void Pause()
		{
			_paused = true;
		}

		public void Resume()
		{
			_paused = false;
		}
		#endregion

		#region private functions
		/// <summary>
		/// Interprets a touch input.
		/// </summary>
		/// <param name="touch"></param>
		private void ProcessTouch(Touch touch)
		{
			switch (touch.phase)
			{
				case TouchPhase.Began: //The finger first touched the screen --> It could be(come) a swipe
					TouchData data = new TouchData(touch.fingerId, touch.position);
					if (_touches.ContainsKey(touch.fingerId))
					{
						_touches.Remove(touch.fingerId);
					}
					_touches.Add(data.FingerId, data);
					//VerifyTouch(data);
					if (HasValidStart(data))
					{
						_onScreenTouched.Invoke(data.FingerId);
					}
					break;
				case TouchPhase.Stationary:
					if (_touches.ContainsKey(touch.fingerId))
					{
						data = _touches[touch.fingerId];
						if (data.Moving && Time.time - data.PositionUpdateTime >= _minTimeToStationary.Value)
						{
							data.Moving = false;
						}

						if(!data.Moving)
						{
							//data.Position = touch.position;
							if (IsValid(data))
							{
								if (!data.IsEligibleForClick(_maxClickTime.Value))
								{
									_onHold.Invoke(data.FingerId);
								}
							}
						}
					}
					break;
				case TouchPhase.Moved:
					if (_touches.ContainsKey(touch.fingerId))
					{
						data = _touches[touch.fingerId];
						//data.Moving = true;
						data.Position = touch.position;
						if (IsValid(data))
						{
							if (!data.IsEligibleForSwipe(_swipeMaxTime.Value, _minSwipeDistance))
							{
								if (data.IsMove(_minMoveDistance))
								{
									_onMove.Invoke(data.FingerId);
								}
								else
								{
									_onHold.Invoke(data.FingerId);
								}
							}
						}
					}
					break;
				case TouchPhase.Canceled:
					if (_touches.ContainsKey(touch.fingerId))
					{
						data = _touches[touch.fingerId];
						if (IsValid(data))
						{
							data.OnScreen = false;
							//data.Position = touch.position;
							if (data.IsSwipe(_swipeMaxTime.Value, _minSwipeDistance))
							{
								RaiseSwipeEvent(data);
							}
							_onScreenUntouched.Invoke(data.FingerId);
						}
						_touches.Remove(touch.fingerId);
					}
					break;
				case TouchPhase.Ended:
					if (_touches.ContainsKey(touch.fingerId))
					{
						data = _touches[touch.fingerId];
						if (IsValid(data))
						{
							data.OnScreen = false;
							//data.Position = touch.position;
							if (data.IsSwipe(_swipeMaxTime.Value, _minSwipeDistance))
							{
								RaiseSwipeEvent(data);
							}
							else if (data.IsEligibleForClick(_maxClickTime.Value))
							{
								_onClick.Invoke(data.FingerId);
							}
							_onScreenUntouched.Invoke(data.FingerId);
						}
						_touches.Remove(touch.fingerId);
					}
					break;
			}
			if (_screenPositions != null && _screenPositions.Length > touch.fingerId)
			{
				_screenPositions[touch.fingerId].Value = touch.position;
			}
		}

		/// <summary>
		/// Identify the direction of the swype and update the swipe event.
		/// </summary>
		/// <param name="data"></param>
		private void RaiseSwipeEvent(TouchData data)
		{
			Vector2 delta = data.Position - data.MovementStartPosition;
			if (Mathf.Abs(delta.y) >= Mathf.Abs(delta.x))
			{
				// Determine if it was a swipe up or down
				if (delta.y > 0)
				{
					_onSwipeUp.Invoke(data.FingerId);
				}
				else
				{
					_onSwipeDown.Invoke(data.FingerId);
				}
			}
			else
			{
				// Determine if it was a swipe up or down
				if (delta.x > 0)
				{
					_onSwipeRight.Invoke(data.FingerId);
				}
				else
				{
					_onSwipeLeft.Invoke(data.FingerId);
				}
			}
		}

		/// <summary>
		/// Updates the pixel drag threshold of the EventSystem based on the screen dpi.
		/// </summary>
		/// <param name="screenDpi"></param>
		private void UpdatePixelDrag(float screenDpi)
		{
			_minSwipeDistance = Mathf.RoundToInt((screenDpi / _referenceDPI.Value) * _swipeMinDistance.Value);
			_minMoveDistance = Mathf.RoundToInt((screenDpi / _referenceDPI.Value) * _moveMinDistance.Value);
		}

		/// <summary>
		/// Gets if the input had a valid start.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private bool HasValidStart(TouchData data)
		{
			// The pause verification is here instead of being in the Update function because we want the update
			// to work even when the game is paused, so the touch information can keep being collected
			if (_paused)
			{
				data.IsValid = false;
			}
			else if (_hudCanvas == null || !IsOverCanvasElement(data.Position, _hudCanvas))
			{
				if (data.OnScreen)
				{
					data.IsValid = true;
				}
				else
				{
					data.IsValid = false;
				}
			}
			return data.IsValid;
		}

		/// <summary>
		/// Is this click a valid hold move.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private bool IsValid(TouchData data)
		{
			if(_paused)
			{
				data.IsValid = false;
			}
			else if (data.IsValid && data.OnScreen)
			{
				data.IsValid = true;
			}
			return data.IsValid;
		}

		/// <summary>
		/// Gets if a screen position is over a canvas element.
		/// </summary>
		/// <param name="position"></param>
		/// <param name="canvas"></param>
		/// <returns></returns>
		private bool IsOverCanvasElement(Vector2 position, Canvas canvas)
		{
			PointerEventData pointerData = new PointerEventData(null);
			pointerData.position = position;
			List<RaycastResult> hudObjects = new List<RaycastResult>();
			canvas.GetComponent<GraphicRaycaster>().Raycast(pointerData, hudObjects);

			return hudObjects.Count > 0;
		}
		#endregion

		#region private class
		/// <summary>
		/// Private class used to store data about a touch in the touchscreen of the device.
		/// </summary>
		private class TouchData
		{
			#region private variables
			private int _fingerId;
			private bool _isValid = true;
			private bool _onScreen;
			private bool _moved = false;
			private List<TouchPosition> _positions = new List<TouchPosition>(2);
			#endregion

			#region public properties
			/// <summary>
			/// TouchData Id.
			/// </summary>
			public int FingerId
			{
				get
				{
					return _fingerId;
				}
			}

			/// <summary>
			/// Gets if the touch is a valid touch.
			/// </summary>
			public bool IsValid
			{
				get
				{
					return _isValid;
				}
				set
				{
					_isValid = value;
				}
			}

			/// <summary>
			/// Bool indicating if the finger that created the touch is still touching the screen.
			/// </summary>
			public bool OnScreen
			{
				get
				{
					return _onScreen;
				}
				set
				{
					_onScreen = value;
				}
			}

			/// <summary>
			/// Gets the position of the touch at the start of the last movement.
			/// </summary>
			public Vector2 MovementStartPosition
			{
				get
				{
					int index = _positions.Count - 1;
					for (int i = _positions.Count - 1; i >= 0; i--)
					{
						if (!_positions[i].Moving)
						{
							break;
						}
						else
						{
							index = i;
						}
					}
					return _positions[index].Position;
				}
			}

			/// <summary>
			/// Screen position of the touch.
			/// </summary>
			public Vector2 Position
			{
				get
				{
					return _positions[_positions.Count - 1].Position;
				}
				set
				{
					//if(_positions.Count > 0)
					//{
					//	Moving = true;
					//}
					_positions.Add(new TouchPosition(value, Time.time, true));
				}
			}

			/// <summary>
			/// Gets the starting position of the touch.
			/// </summary>
			public Vector2 StartingPosition
			{
				get
				{
					return _positions[0].Position;
				}
			}

			/// <summary>
			/// Get the time the finger touched the screen.
			/// </summary>
			public float TouchStartTime
			{
				get
				{
					return _positions[0].CreationTime;
				}
			}

			/// <summary>
			/// Gets when the touch started moving.
			/// </summary>
			public float MovementStartTime
			{
				get
				{
					int index = _positions.Count - 1;
					for(int i = _positions.Count - 1; i >= 0; i--)
					{
						if(!_positions[i].Moving)
						{
							break;
						}
						else
						{
							index = i;
						}
					}
					return _positions[index].CreationTime;
				}
			}

			/// <summary>
			/// Gets the last time the position was updated.
			/// </summary>
			public float PositionUpdateTime
			{
				get
				{
					return _positions[_positions.Count - 1].CreationTime;
				}
			}

			/// <summary>
			/// Gets true if the finger moved since it started touching the screen.
			/// </summary>
			public bool Moved
			{
				get
				{
					return _moved;
				}
			}

			/// <summary>
			/// Gets true if the finger moved since the last frame.
			/// </summary>
			public bool Moving
			{
				get
				{
					if(_positions.Count == 0)
					{
						return false;
					}
					return _positions[_positions.Count - 1].Moving;
				}
				set
				{
					if(value)
					{
						_moved = true;
					}
					_positions[_positions.Count - 1].SetMoving(value);
				}
			}
			#endregion

			#region public functions
			/// <summary>
			/// Initializes a new instance of the <see cref="Mariachi.ElGordo.CharacterDefaultController+TouchData"/> 
			/// class, used by the CharacterDefaultController to keep track of all the touch inputs the user does.
			/// </summary>
			/// <param name="id">Identifier.</param>
			/// <param name="position">Position.</param>
			public TouchData(int id, Vector2 position)
			{
				_fingerId = id;
				_onScreen = true;
				Position = position;
			}

			/// <summary>
			/// Gets if this input can still be a click.
			/// </summary>
			/// <param name="maxClickTime"></param>
			/// <returns></returns>
			public bool IsEligibleForClick(float maxClickTime)
			{
				return !Moved && Time.time - TouchStartTime < maxClickTime;
			}

			/// <summary>
			/// Gets if a moving touch is elligible to be a swipe.
			/// </summary>
			/// <param name="maxSwipeTime"></param>
			/// <param name="minSwipeDistance"></param>
			/// <returns></returns>
			public bool IsEligibleForSwipe(float maxSwipeTime, float minSwipeDistance)
			{
				float movementVelocity = (Position - MovementStartPosition).magnitude / (Time.time - MovementStartTime);
				float swipeMinVelocity = minSwipeDistance / maxSwipeTime;
				return Moving && Time.time - MovementStartTime < maxSwipeTime && movementVelocity >= swipeMinVelocity;
			}

			/// <summary>
			/// Gets if a move is a swipe.
			/// </summary>
			/// <param name="maxSwipeTime"></param>
			/// <param name="minSwipeDistance"></param>
			/// <returns></returns>
			public bool IsSwipe(float maxSwipeTime, float minSwipeDistance)
			{
				return IsEligibleForSwipe(maxSwipeTime, minSwipeDistance) &&
					(Position - MovementStartPosition).sqrMagnitude >= minSwipeDistance * minSwipeDistance;
			}

			/// <summary>
			/// Gets if the movement was enough to be considered a move.
			/// </summary>
			/// <param name="minMoveDistance"></param>
			/// <returns></returns>
			public bool IsMove(float minMoveDistance)
			{
				return (Position - MovementStartPosition).sqrMagnitude >= minMoveDistance * minMoveDistance;
			}
			#endregion

			#region private structs
			/// <summary>
			/// Holds data about a touch position.
			/// </summary>
			private class TouchPosition
			{
				#region private attributes
				private bool _moving;
				#endregion

				#region public properties
				/// <summary>
				/// Gets the position of the touch.
				/// </summary>
				public Vector2 Position { get; private set; }

				/// <summary>
				/// Gets the time the touch position object was created.
				/// </summary>
				public float CreationTime { get; private set; }

				/// <summary>
				/// Gets if this touch position is moving.
				/// </summary>
				public bool Moving
				{
					get
					{
						return _moving;
					}
					set
					{
						_moving = value;
					}
				}
				#endregion

				#region public functions
				/// <summary>
				/// Instantiates a new touch position object.
				/// </summary>
				/// <param name="position"></param>
				/// <param name="time"></param>
				/// <param name="moving"></param>
				public TouchPosition(Vector2 position, float time, bool moving = false)
				{
					Position = position;
					CreationTime = time;
					_moving = moving;
				}

				/// <summary>
				/// Sets the touch as moving.
				/// </summary>
				/// <param name="moving"></param>
				public void SetMoving(bool moving)
				{
					Moving = moving;
				}
				#endregion
			}
			#endregion
		}
		#endregion
	}
}