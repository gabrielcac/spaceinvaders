﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Holds a collection of <see cref="GameObject"/>s during runtme.
	/// </summary>
	public abstract class RunTimeSet : ScriptableObject, ICollection<GameObject>
	{
		#region private attributes
#pragma warning disable CS0414
		[SerializeField]
		[Multiline]
		private string _description = null;
#pragma warning restore CS0414
		[SerializeField]
		private GameEvent _onUpdateEvent = null;
		#endregion

		#region public properties
		public virtual int Count
		{
			get
			{
				return EditableSet.Count;
			}
		}
		
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		public virtual object SyncRoot
		{
			get
			{
				return false;
			}
		}

		public virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}
		#endregion

		#region protected properties
		/// <summary>
		/// The editable collection of objects of this set.
		/// </summary>
		protected abstract ICollection<GameObject> EditableSet { get; }
		#endregion

		#region ICollection implementation
		public virtual void Add(GameObject member)
		{
			EditableSet.Add(member);
			if(_onUpdateEvent != null)
			{
				_onUpdateEvent.Raise();
			}
		}

		public virtual void Clear()
		{
			EditableSet.Clear();
		}

		public virtual bool Contains(GameObject item)
		{
			return EditableSet.Contains(item);
		}

		public virtual void CopyTo(GameObject[] array, int index)
		{
			EditableSet.CopyTo(array, index);
		}

		public virtual bool Remove(GameObject member)
		{
			if (EditableSet.Remove(member))
			{
				if (_onUpdateEvent != null)
				{
					_onUpdateEvent.Raise();
				}
				return true;
			}
			return false;
		}

		IEnumerator<GameObject> IEnumerable<GameObject>.GetEnumerator()
		{
			return EditableSet.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return EditableSet.GetEnumerator();
		}
		#endregion
	}
}