﻿using System.Collections.Generic;
using UnityEngine;

namespace PandB
{
	/// <summary>
	/// An implementation of a <see cref="RunTimeSet"/> as a list.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.gameListFileName, menuName = CreateMenus.gameListMenu,
		order = CreateMenus.gameListOrder)]
	public class RunTimeList : RunTimeSet, IList<GameObject>
	{
		#region private attributes
		private List<GameObject> _list = new List<GameObject>();
		#endregion

		#region public properties
		public GameObject this[int index]
		{
			get
			{
				return _list[index];
			}
			set
			{
				_list[index] = value;
			}
		}

		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}
		#endregion

		#region protected properties
		protected override ICollection<GameObject> EditableSet
		{
			get
			{
				return _list;
			}
		}
		#endregion

		#region IList implementation
		public int IndexOf(GameObject value)
		{
			return _list.IndexOf(value);
		}

		public void Insert(int index, GameObject value)
		{
			_list.Insert(index, value);
		}

		public void RemoveAt(int index)
		{
			_list.RemoveAt(index);
		}
		#endregion
	}
}