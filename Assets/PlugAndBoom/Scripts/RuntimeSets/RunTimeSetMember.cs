﻿using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Member of a <see cref="RunTimeSet"/>.
	/// </summary>
	public class RunTimeSetMember : MonoBehaviour
	{
		#region public enums
		public enum Activation { OnAwake, OnEnable, OnStart, OnDisable, None }
		#endregion

		#region private attributes
		[SerializeField]
		private RunTimeSet _set = null;
		[SerializeField]
		private Activation _joinSet = Activation.OnEnable;
		[Tooltip("The member is automatically removed from the set OnDestroy, if it hasn't been removed earlier.")]
		[SerializeField]
		private Activation _leaveSet = Activation.OnDisable;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			if(_joinSet == Activation.OnAwake)
			{
				_set.Add(gameObject);
			}
			if(_leaveSet == Activation.OnAwake)
			{
				_set.Remove(gameObject);
			}
		}

		private void OnEnable()
		{
			if (_joinSet == Activation.OnEnable)
			{
				_set.Add(gameObject);
			}
			if (_leaveSet == Activation.OnEnable)
			{
				_set.Remove(gameObject);
			}
		}

		private void Start()
		{
			if (_joinSet == Activation.OnStart)
			{
				_set.Add(gameObject);
			}
			if (_leaveSet == Activation.OnStart)
			{
				_set.Remove(gameObject);
			}
		}

		private void OnDisable()
		{
			if (_joinSet == Activation.OnDisable)
			{
				_set.Add(gameObject);
			}
			if (_leaveSet == Activation.OnDisable)
			{
				_set.Remove(gameObject);
			}
		}

		private void OnDestroy()
		{
			_set.Remove(gameObject);
		}
		#endregion

		#region public functions
		/// <summary>
		/// Adds the gameObject to the configured <see cref="RunTimeSet"/>.
		/// </summary>
		public void AddToSet()
		{
			_set.Add(gameObject);
		}

		/// <summary>
		/// Removes the gameobject from the configured <see cref="RunTimeSet"/>.
		/// </summary>
		public void RemoveFromSet()
		{
			_set.Remove(gameObject);
		}
		#endregion
	}
}