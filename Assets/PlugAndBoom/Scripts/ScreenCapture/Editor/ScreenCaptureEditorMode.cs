﻿using UnityEngine;
using UnityEditor;

namespace PandB.ScreenCapture
{
	/// <summary>
	/// Handles the composition of the inspector of the <see cref="Screenshot"/>.
	/// </summary>
	[CustomEditor(typeof(Screenshot))]
	public class ScreenCaptureEditorMode : Editor
	{
		#region Editor implementation
		public override void OnInspectorGUI()
		{
			Screenshot screenshot = target as Screenshot;

			DrawDefaultInspector();

			GUI.enabled = screenshot.enabled;

			if (GUILayout.Button("Screenshot (O)"))
			{
				screenshot.CaptureShot();
			}

			EditorGUILayout.Separator();

			GUI.enabled = Application.isPlaying;

			string recordButton = "Record Video (P)";
			if (screenshot.Recording)
			{
				recordButton = "Stop Recording (P)";
			}
			if (GUILayout.Button(recordButton))
			{
				screenshot.Rec();
			}

			GUI.enabled = true;

			EditorUtility.SetDirty(target);
		}
		#endregion
	}
}