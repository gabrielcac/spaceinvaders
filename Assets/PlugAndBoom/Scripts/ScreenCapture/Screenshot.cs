﻿using System.IO;
using UnityEngine;

namespace PandB.ScreenCapture
{
	/// <summary>
	/// Takes screenshots and saves game frames for video composition.
	/// </summary>
	public class Screenshot : MonoBehaviour
	{
#if UNITY_EDITOR
		#region public attributes
		[Header("General Configuration")]
		[Tooltip("Factor by which to increase the screenshot resolution")]
		public int screenshotSize = 1;
		[Header("Screenshot")]
		public string screenshotName = "Screenshot";
		[Header("Video")]
		public string videoName = "Video";
		#endregion

		#region private attributes
		private bool _recordVideo;
		private int _shotCount;
		private int _frameCount;
		#endregion

		#region public properties
		/// <summary>
		/// Gets if the script is recording the gameplay.
		/// </summary>
		public bool Recording
		{
			get
			{
				return _recordVideo;
			}
		}
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			Application.targetFrameRate = 24;
			_recordVideo = false;
			if (!Directory.Exists("Screenshots")) //check if "Screenshots" folder exists
			{
				Directory.CreateDirectory("Screenshots");
			}
			if (!Directory.Exists("VideoCaptures")) //check if "VideoCaptures" folder exists
			{
				Directory.CreateDirectory("VideoCaptures");
			}
		}

		private void LateUpdate()
		{
			if (_recordVideo)
			{
				SaveScreenShot("VideoCaptures/" + videoName + _frameCount + ".png", screenshotSize);
				_frameCount++;
			}
			if (Input.GetKeyDown(KeyCode.P))
			{
				Rec();
			}
			if (Input.GetKeyDown(KeyCode.O))
			{
				CaptureShot();
			}
		}
		#endregion

		#region public functions
		/// <summary>
		/// Start/Stop saving screenshots for a video.
		/// </summary>
		public void Rec()
		{
			if (_recordVideo)
			{
				_recordVideo = false;
				Debug.Log("Rec - Stopped");
				return;
			}
			else
			{
				_recordVideo = true;
				Debug.Log("Rec - Started");
				return;
			}
		}

		/// <summary>
		/// Captures a screenshot.
		/// </summary>
		public void CaptureShot()
		{
			SaveScreenShot("Screenshots/" + screenshotName + _shotCount + ".png", screenshotSize);
			Debug.Log("Screenshot taken");
			_shotCount++;
		}
		#endregion

		#region private functions
		/// <summary>
		/// Captures a single screenshot and save it to the path.
		/// Updates the counter after capturing the image.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="size"></param>
		public void SaveScreenShot(string path, int size)
		{
			UnityEngine.ScreenCapture.CaptureScreenshot(path, size);
		}
		#endregion
#endif
	}
}