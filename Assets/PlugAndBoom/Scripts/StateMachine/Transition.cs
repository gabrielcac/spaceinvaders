namespace PandB.StateMachine
{
	/// <summary>
	/// Transition between states of the generic Plug & Boom State Machine class.
	/// It has two attributes, the state to wich it belongs and the state id of the state the machine state ends 
	/// after the transition.
	/// If the transition occurs, the OnTransition method is called. This method should be implemented by whoever
	/// inherits this class.
	/// </summary>
	public class Transition
	{
		#region private variables
		private State _state;
		private int _destinyStateId;
		#endregion

		#region public properties
		/// <summary>
		/// Gets the destination state identifier.
		/// </summary>
		/// <value>The destiny state identifier.</value>
		public int DestinyStateId
		{
			get
			{
				return _destinyStateId;
			}
		}
		#endregion

		#region protected properties
		/// <summary>
		/// Gets the state from where this transition starts.
		/// </summary>
		/// <value>The state.</value>
		protected State State
		{
			get
			{
				return _state;
			}
		}

		/// <summary>
		/// Gets the destination state.
		/// </summary>
		/// <value>The state of the destiny.</value>
		protected State DestinyState
		{
			get
			{
				// TODO Search the state machine for a state with this id and return it
				return null;
			}
		}

		/// <summary>
		/// Gets the state machine to wich this transition belongs.
		/// </summary>
		/// <value>The state machine.</value>
		protected IStateMachine StateMachine
		{
			get
			{
				return _state.StateMachine;
			}
		}
		#endregion

		#region public functions
		/// <summary>
		/// Initializes a new instance of the <see cref="PandB.StateMachine.Transition"/> class.
		/// </summary>
		/// <param name="state">State from where the transitions starts.</param>
		/// <param name="destinyStateId">Destination state identifier.</param>
		public Transition(State state, int destinyStateId)
		{
			_state = state;
			_destinyStateId = destinyStateId;
		}

		/// <summary>
		/// <para>DON'T CALL THIS METHOD!</para>
		/// <para>Called by the StateMachine that handles this transition when the transition happens. If you need this 
		/// transition to execute a particular action, override this method.</para>
		/// <para>This method is called by the StateMachine that handles this transition.</para>
		/// </summary>
		/// <param name="args"></param>
		public virtual void OnTransition(StateMachineArgs args = null)
		{
		}
		#endregion
	}
}