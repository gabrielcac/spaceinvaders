using UnityEngine;
using System.Collections.Generic;

namespace PandB.StateMachine
{
	/// <summary>
	/// Plug & Boom generic state machine. This can be used to control scenes, characters, UIs,whatever needs the state 
	/// machine pattern.
	/// The state machine will initiate itself using MonoBehavior Start function.
	/// To attempt a state change call the function ChangeTo, with the id of the new state. If the transition is valid
	/// the state machine will be updated.
	/// </summary>
	public abstract class StateMachine : IStateMachine
    {
		#region private variables
		private State _currentState;
		private Dictionary<int, State> _states;
		#endregion

		#region public properties
		/// <summary>
		/// Gets the current state of the state machine.
		/// </summary>
		/// <value>The current state.</value>
		public State CurrentState
		{
			get
			{
				return _currentState;
			}
		}
		#endregion

		#region protected properties
		/// <summary>
		/// Gets all the states of this machine.
		/// </summary>
		/// <value>The states.</value>
		protected Dictionary<int, State> States
		{
			get
			{
				return _states;
			}
		}

		/// <summary>
		/// <para>Gets the machine starting state.</para>
		/// <para>This default implementation returns the state with the lowest positive id as the starting state. If
		/// this is not the intended starting state, override this function.</para>
		/// </summary>
		/// <value>The first state of the state machine.</value>
		protected virtual State StartingState
		{
			get
			{
				for(int i = 0, j = States.Count; i < j; i++)
				{
					if(States[i] != null)
					{
						return States[i];
					}
				}
				return null;
			}
		}
		#endregion

		#region public functions
		/// <summary>
		/// Start this instance, creating all the State objects and setting the state machine to its initial state.
		/// </summary>
		/// <param name="args"></param>
		public virtual void StartStateMachine(StateMachineArgs args = null)
		{
			_states = new Dictionary<int, State>();
			CreateStates(ref _states);

			// Set the starting state as the current state
			StartingState.OnStateEnter(args);
			_currentState = StartingState;
		}

		/// <summary>
		/// <para>Attempt to change state.</para>
		/// <para>If the attempted transition is not valid, print an error message and returns false.</para>
		/// <para>This function should never be called outside the <see cref="StateMachine"/> implementation.</para>
		/// </summary>
		/// <returns><c>true</c>, if the state was changed, <c>false</c> otherwise.</returns>
		/// <param name="newStateId">Unique identifier of the state to change to.</param>
		/// <param name="args"></param>
		public virtual bool ChangeTo(int newStateId, StateMachineArgs args = null)
		{

			State newState = States[newStateId];
			if (newState == null)
			{
				Debug.LogError("Someone attempted to make and invalid transition on the " + GetType().Name + " State Machine.\n" +
							   "The targeted state with id" + newStateId + " doesn't exist.");
				return false;
			}

			if (!CurrentState.Transitions.ContainsKey(newStateId))
			{
				Debug.LogWarning("Someone attempted to make and invalid transition on the " + GetType().Name + " State Machine.\n" +
				                 "The transition was between states " + _currentState.GetType().Name + " and " + newState.GetType().Name + ".");
				return false;
			}
			
			_currentState.OnStateLeave(args);
			_currentState.Transitions[newState.Id].OnTransition();
			//Debug.Log(GetType().Name + " (" + GetHashCode() + ") " + "on " + Time.time + " changing from state " + _currentState.GetType().Name + " to " + newState.GetType().Name);
			_currentState = newState;
			_currentState.OnStateEnter(args);

			return true;
		}

		/// <summary>
		/// Called when the state machine must be warned about an event, such as an animation ending or the
		/// loading of a scene.
		/// </summary>
		/// <param name="eventName">Action Name.</param>
		/// <param name="args"></param>
		public void Event(int eventId, StateMachineArgs args = null)
		{
            if (CurrentState != null)
            {
		    	CurrentState.OnEvent(eventId, args);
            }
        }
		#endregion

		#region protected functions
		/// <summary>
		/// Creates all the states of this machine and adds them to the states dictionary.
		/// </summary>
		/// <param name="states">States.</param>
		protected abstract void CreateStates(ref Dictionary<int, State> states);
		#endregion

		#region private functions
		private void SetCurrentState(State state, StateMachineArgs args)
		{
			// If this is the starting state of the state machine
			if (_currentState == null)
			{
				state.OnStateEnter(args);
				_currentState = state;
				return;
			}

			_currentState.OnStateLeave(args);
			_currentState.Transitions[state.Id].OnTransition();
			_currentState = state;
			_currentState.OnStateEnter(args);
		}
		#endregion
	}
}