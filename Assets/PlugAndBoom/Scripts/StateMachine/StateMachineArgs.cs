﻿namespace PandB.StateMachine
{
	/// <summary>
	/// Abstract class to hold arguments between state machine transitions.
	/// </summary>
	public abstract class StateMachineArgs
	{
	}
}