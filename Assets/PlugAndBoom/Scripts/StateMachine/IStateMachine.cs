﻿namespace PandB.StateMachine
{
	public interface IStateMachine
	{
		#region public properties
		/// <summary>
		/// Gets the current state of the state machine.
		/// </summary>
		/// <value>The current state.</value>
		State CurrentState
		{
			get;
		}
		#endregion

		#region public functions
		/// <summary>
		/// Start this instance, creating all the State objects and setting the state machine to its initial state.
		/// </summary>
		/// <param name="args"></param>
		void StartStateMachine(StateMachineArgs args = null);

		/// <summary>
		/// <para>Attempt to change state.</para>
		/// <para>If the attempted transition is not valid, print an error message and returns false.</para>
		/// <para>This function should never be called outside the <see cref="StateMachine"/> implementation.</para>
		/// </summary>
		/// <returns><c>true</c>, if the state was changed, <c>false</c> otherwise.</returns>
		/// <param name="newStateId">Unique identifier of the state to change to.</param>
		/// <param name="args"></param>
		bool ChangeTo(int newStateId, StateMachineArgs args = null);

		/// <summary>
		/// Called when the state machine must be warned about an event, such as an animation ending or the
		/// loading of a scene.
		/// </summary>
		/// <param name="eventName">Action Name.</param>
		/// <param name="args"></param>
		void Event(int eventId, StateMachineArgs args = null);
		#endregion
	}
}