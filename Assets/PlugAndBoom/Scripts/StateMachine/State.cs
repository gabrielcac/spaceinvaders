using System.Collections.Generic;

namespace PandB.StateMachine
{
	/// <summary>
	/// A state from the Plug & Boom State Machine.
	/// To inherit this class you will need to override the OnStateEnter and OnStateLeave methods. These are called 
	/// when the state machine enters and leaves this state.
	/// To instantiate a new state object you must provide the state machine object that owns this state and an unique 
	/// id. The id must be unique for all states of the given state machine.
	/// Transitions from this state to others states should be added to the state object once it is instantiated.
	/// To do that, create as many <see cref="Transition"/> objects as needed and add them to the
	/// state using the AddTransition method.
	/// </summary>
	public abstract class State
	{
		#region private variables
		private IStateMachine _stateMachine;
		private Dictionary<int, Transition> _transitions;
		#endregion

		#region public properties
		/// <summary>
		/// Gets the state machine to wich this transition belongs.
		/// </summary>
		/// <value>The state machine.</value>
		public IStateMachine StateMachine
		{
			get
			{
				return _stateMachine;
			}
		}

		/// <summary>
		/// Gets the state unique identifier.
		/// </summary>
		/// <value>The identifier.</value>
		public virtual int Id
		{
			get
			{
				return -1;
			}
		}

		/// <summary>
		/// List of transitions leaving this state.
		/// </summary>
		public Dictionary<int, Transition> Transitions
		{
			get
			{
				return _transitions;
			}
		}
		#endregion

		#region public functions
		/// <summary>
		/// Initializes a new instance of the <see cref="State"/> class.
		/// </summary>
		/// <param name="stateMachine">State machine that owns the state.</param>
		public State(IStateMachine stateMachine)
		{
			_stateMachine = stateMachine;
		}

		/// <summary>
		/// <para>DON'T CALL THIS METHOD!</para>
		/// <para>Called by the IStateMachine that handles this state when the state is set as the current active
		/// state.</para>
		/// </summary>
		/// <param name="args"></param>
		public abstract void OnStateEnter(StateMachineArgs args = null);

		/// <summary>
		/// <para>DON'T CALL THIS METHOD!</para>
		/// <para>Called by the IStateMachine that handles this state at every render loop while this state is active.
		/// </para>
		/// </summary>
		public virtual void Update()
		{
			// Does nothing
		}

		/// <summary>
		/// <para>DON'T CALL THIS METHOD!</para>
		/// <para>Called by the IStateMachine that handles this state at every interation of the physics engine while this 
		/// state is active.</para>
		/// </summary>
		public virtual void FixedUpdate()
		{
			// Does nothing
		}

		/// <summary>
		/// <para>DON'T CALL THIS METHOD!</para>
		/// <para>Called by the IStateMachine that handles this state when the state is no longer set as the current
		/// active state.</para>
		/// </summary>
		/// <param name="args"></param>
		public abstract void OnStateLeave(StateMachineArgs args = null);

		/// <summary>
		/// Called when an action in the game happens and the state machine is notified to take action.
		/// </summary>
		/// <param name="eventId">id of the event as specified by the state machine.</param>
		/// <param name="args"></param>
		public virtual void OnEvent(int eventId, StateMachineArgs args = null)
		{
			// Does nothing
		}
		#endregion

		#region protected functions
		/// <summary>
		/// Adds a transition from this state to another state specified in the Transition object.
		/// </summary>
		/// <param name="transition">Transition.</param>
		protected void AddTransition(Transition transition)
		{
			if(_transitions == null)
			{
				_transitions = new Dictionary<int, Transition>();
			}
			_transitions.Add(transition.DestinyStateId, transition);
		}
		#endregion
	}
}