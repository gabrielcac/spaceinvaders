﻿using UnityEditor;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how an <see cref="AnimationCurveReference"/> is displayed in the Unity Editor.
	/// </summary>
	[CustomPropertyDrawer(typeof(AnimationCurveReference))]
	public class AnimationCurveReferenceProperty : VariableReferenceProperty<AnimationCurve>
	{
	}
}