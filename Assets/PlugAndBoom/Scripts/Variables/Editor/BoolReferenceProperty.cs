﻿using UnityEditor;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how a <see cref="BoolReference"/> is displayed in the Unity Editor.
	/// </summary>
	[CustomPropertyDrawer(typeof(BoolReference))]
	public class BoolReferenceProperty : VariableReferenceProperty<bool>
	{
	}
}