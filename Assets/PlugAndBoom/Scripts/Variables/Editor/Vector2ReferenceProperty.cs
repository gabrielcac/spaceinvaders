﻿using UnityEditor;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how a <see cref="Vector2Reference"/> is displayed in the Unity Editor.
	/// </summary>
	[CustomPropertyDrawer(typeof(Vector2Reference))]
	public class Vector2ReferenceProperty : VariableReferenceProperty<Vector2>
	{
	}
}