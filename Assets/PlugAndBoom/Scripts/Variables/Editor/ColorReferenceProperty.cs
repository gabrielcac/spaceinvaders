﻿using UnityEditor;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how a <see cref="ColorReference"/> is displayed in the Unity Editor.
	/// </summary>
	[CustomPropertyDrawer(typeof(ColorReference))]
	public class ColorReferenceProperty : VariableReferenceProperty<Color>
	{
	}
}