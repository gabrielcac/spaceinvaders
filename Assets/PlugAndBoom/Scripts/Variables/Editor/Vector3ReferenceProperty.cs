﻿using UnityEditor;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how a <see cref="Vector3Reference"/> is displayed in the Unity Editor.
	/// </summary>
	[CustomPropertyDrawer(typeof(Vector3Reference))]
	public class Vector3ReferenceProperty : VariableReferenceProperty<Vector3>
	{
	}
}