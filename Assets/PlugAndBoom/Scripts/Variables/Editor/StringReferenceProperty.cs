﻿using UnityEditor;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how a <see cref="StringReference"/> is displayed in the Unity Editor.
	/// </summary>
	[CustomPropertyDrawer(typeof(StringReference))]
	public class StringReferenceProperty : VariableReferenceProperty<string>
	{
	}
}