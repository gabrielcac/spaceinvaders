﻿using UnityEditor;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how a <see cref="FloatReference"/> is displayed in the Unity Editor.
	/// </summary>
	[CustomPropertyDrawer(typeof(FloatReference))]
	public class FloatReferenceProperty : VariableReferenceProperty<float>
	{
	}
}