﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how a VariableReference is displayed by the Unity Editor.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class VariableReferenceProperty<T> : PropertyDrawer
	{
		#region private attributes
		//private PropertyData _propertyData;
		private Dictionary<string, PropertyData> _propertyDataPerPropertyPath = new Dictionary<string, PropertyData>();
		#endregion

		#region PropertyDrawer functions
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			PropertyData data = Init(property);

			EditorGUI.BeginProperty(position, label, property);

			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

			Rect fieldRect = position;

			fieldRect.width = EditorGUIUtility.singleLineHeight;
#if UNITY_2019_3_OR_NEWER
			if (GUI.Button(fieldRect, "", (GUIStyle)"FoldoutHeaderIcon"))
#else
			if (GUI.Button(fieldRect, "", (GUIStyle)"Icon.TrackOptions"))
#endif
			{
				ProcessOptionsMenu(property, data);
			}

			fieldRect.position = new Vector2(position.x + fieldRect.width + 2, position.y);
			fieldRect.width = position.width + position.x - fieldRect.position.x;

			if (data.useConstant.boolValue)
			{
				EditorGUI.PropertyField(fieldRect, data.constant, GUIContent.none);
			}
			else
			{
				EditorGUI.PropertyField(fieldRect, data.variable, GUIContent.none);
			}

			EditorGUI.EndProperty();
		}
		#endregion

		#region private functions
		private PropertyData Init(SerializedProperty property)
		{
			PropertyData data = null;
			if (_propertyDataPerPropertyPath.TryGetValue(property.propertyPath, out data))
			{
				return data;
			}

			data = new PropertyData()
			{
				useConstant = property.FindPropertyRelative("useConstant"),
				constant = property.FindPropertyRelative("constant"),
				variable = property.FindPropertyRelative("variable")
			};

			_propertyDataPerPropertyPath.Add(property.propertyPath, data);
			return data;
		}

		private void ProcessOptionsMenu(SerializedProperty property, PropertyData data)
		{
			GenericMenu genericMenu = new GenericMenu();
			bool useConstant = data.useConstant.boolValue;
			genericMenu.AddItem(new GUIContent("Use Constant"), useConstant,
				() => {
					data.useConstant.boolValue = true;
					property.serializedObject.ApplyModifiedProperties();
				});
			genericMenu.AddItem(new GUIContent("Use Variable"), !useConstant, 
					() => {
						data.useConstant.boolValue = false;
						property.serializedObject.ApplyModifiedProperties();
					});
			genericMenu.ShowAsContext();
		}
		#endregion

		#region private classes
		private class PropertyData
		{
			public SerializedProperty useConstant;
			public SerializedProperty constant;
			public SerializedProperty variable;
		}
		#endregion
	}
}