﻿using UnityEditor;

namespace PandB.Variables
{
	/// <summary>
	/// Handles how a <see cref="IntReference"/> is displayed in the Unity Editor.
	/// </summary>
	[CustomPropertyDrawer(typeof(IntReference))]
	public class IntReferenceProperty : VariableReferenceProperty<int>
	{
	}
}