﻿using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A <see cref="Variable{T}"/> that stores a Float value.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.floatVariableFileName, menuName = CreateMenus.floatVariableMenu, order = CreateMenus.floatVariableOrder)]
	public class FloatVariable : Variable<float>
	{
	}
}