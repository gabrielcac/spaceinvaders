﻿using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A <see cref="Variable{T}"/> that stores a <see cref="Vector2"/>.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.vector2VariableFileName, menuName = CreateMenus.vector2VariableMenu, order = CreateMenus.vector2VariableOrder)]
	public class Vector2Variable : Variable<Vector2>
	{
	}
}