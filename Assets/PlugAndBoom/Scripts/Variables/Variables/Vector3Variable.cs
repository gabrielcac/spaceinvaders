﻿using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A <see cref="Variable{T}"/> that stores a <see cref="Vector3"/>.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.vector3VariableFileName, menuName = CreateMenus.vector3VariableMenu, order = CreateMenus.vector3VariableOrder)]
	public class Vector3Variable : Variable<Vector3>
	{
	}
}