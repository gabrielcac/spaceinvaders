﻿using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A <see cref="Variable{T}"/> that stores an <see cref="AnimationCurve"/>.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.curveVariableFileName, menuName = CreateMenus.curveVariableMenu, order = CreateMenus.curveVariableOrder)]
	public class AnimationCurveVariable : Variable<AnimationCurve>
	{
	}
}