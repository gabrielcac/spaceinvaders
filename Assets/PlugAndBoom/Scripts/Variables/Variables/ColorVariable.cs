﻿using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A <see cref="Variable{T}"/> that stores a <see cref="Color"/>.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.colorVariableFileName, menuName = CreateMenus.colorVariableMenu, order = CreateMenus.colorVariableOrder)]
	public class ColorVariable : Variable<Color>
	{
	}
}