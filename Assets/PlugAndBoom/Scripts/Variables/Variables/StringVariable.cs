﻿using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A <see cref="Variable{T}"/> that stores a string.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.stringVariableFileName, menuName = CreateMenus.stringVariableMenu, order = CreateMenus.stringVariableOrder)]
	public class StringVariable : Variable<string>
	{
	}
}