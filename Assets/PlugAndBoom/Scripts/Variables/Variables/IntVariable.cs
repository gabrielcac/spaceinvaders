﻿using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A <see cref="Variable{T}"/> that stores an Integer value.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.intVariableFileName, menuName = CreateMenus.intVariableMenu, order = CreateMenus.intVariableOrder)]
	public class IntVariable : Variable<int>
	{
	}
}