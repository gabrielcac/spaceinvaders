﻿using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A <see cref="Variable{T}"/> that stores a bool value.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.boolVariableFileName, menuName = CreateMenus.boolVariableMenu, order = CreateMenus.boolVariableOrder)]
	public class BoolVariable : Variable<bool>
	{
	}
}