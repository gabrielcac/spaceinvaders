﻿using System;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A Variable object stores a value defined by the game designer.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	public abstract class Variable<T> : ScriptableObject, ISerializationCallbackReceiver
	{
		#region private attributes
#pragma warning disable CS0414
		[SerializeField]
		[Multiline]
		private string _description = null;
#pragma warning restore CS0414

		[SerializeField]
#pragma warning disable CS0649
		private T _value;
#pragma warning restore CS0649
		[SerializeField]
		private GameEvent _onUpdatedEvent = null;

		private T _runtimeValue;
		#endregion

		#region public properties
		/// <summary>
		/// Gets the Variable value.
		/// </summary>
		public virtual T Value
		{
			get
			{
#if UNITY_EDITOR
				if (Application.isPlaying)
				{
#endif
					return _runtimeValue;
#if UNITY_EDITOR
				}
				return _value;
#endif
			}
			set
			{
#if UNITY_EDITOR
				if (Application.isPlaying)
				{
#endif
					_runtimeValue = value;
#if UNITY_EDITOR
				}
				else
				{
					_value = value;
				}
#endif

				if (_onUpdatedEvent != null)
				{
					_onUpdatedEvent.Raise();
				}
			}
		}

		/// <summary>
		/// The Variable initial value at the start of the simulation.
		/// </summary>
		public virtual T InitialValue
		{
			get
			{
				return _value;
			}
		}
		#endregion

		#region ISerializationCallbackReceiver implementation
		public virtual void OnAfterDeserialize()
		{
			_runtimeValue = _value;
		}

		public virtual void OnBeforeSerialize()
		{
		}
		#endregion
	}
}