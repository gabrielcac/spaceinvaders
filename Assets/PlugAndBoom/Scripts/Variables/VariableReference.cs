﻿using System;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// A VariableReference object allows the use of a <see cref="Variable{T}"/> or a constant value.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	public abstract class VariableReference<T, U> where U : Variable<T>
	{
		#region private attributes
		[SerializeField]
		private bool useConstant = true;
		[SerializeField]
		private T constant;
		[SerializeField]
#pragma warning disable CS0649
		private U variable;
#pragma warning restore CS0649
		#endregion

		#region public properties
		/// <summary>
		/// Gets the value of the <see cref="Variable{T}"/> or constant referenced by this object.
		/// </summary>
		public T Value
		{
			get
			{
				if (useConstant)
				{
					return constant;
				}
				if(variable == null)
				{
					return constant;
				}
				return variable.Value;
			}
			set
			{
				if (useConstant)
				{
					constant = value;
				}
				if(variable != null)
				{
					variable.Value = value;
				}
			}
		}
		#endregion

		#region public static functions
		/// <summary>
		/// Allows the VariableReference value to be used without the need to reference the Value property.
		/// </summary>
		/// <param name="reference"></param>
		public static implicit operator T(VariableReference<T, U> reference)
		{
			return reference.Value;
		}
		#endregion

		#region public functions
		public VariableReference()
		{
			useConstant = true;
		}

		/// <summary>
		/// Instantiates an object with the specified value.
		/// </summary>
		/// <param name="value"></param>
		public VariableReference(T value)
		{
			useConstant = true;
			constant = value;
		}

		public override string ToString()
		{
			return Value.ToString();
		}
		#endregion
	}
}