﻿using System;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// Allows the use of an <see cref=ColorVariable"/> or a constant <see cref="Color"/>.
	/// </summary>
	[Serializable]
	public class ColorReference : VariableReference<Color, ColorVariable>
	{
		#region VariableReference implementation
		public ColorReference(Color value) : base(value)
		{
		}
		#endregion
	}
}