﻿using System;

namespace PandB.Variables
{
	/// <summary>
	/// Allows the use of a <see cref=FloatVariable"/> or a constant float.
	/// </summary>
	[Serializable]
	public class FloatReference : VariableReference<float, FloatVariable>
	{
		#region VariableReference implementation
		public FloatReference(float value) : base(value)
		{
		}
		#endregion
	}
}