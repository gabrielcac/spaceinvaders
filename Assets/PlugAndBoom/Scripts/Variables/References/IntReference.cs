﻿using System;

namespace PandB.Variables
{
	/// <summary>
	/// Allows the use of an <see cref=IntVariable"/> or a constant integer.
	/// </summary>
	[Serializable]
	public class IntReference : VariableReference<int, IntVariable>
	{
		#region VariableReference implementation
		public IntReference(int value) : base(value)
		{
		}
		#endregion
	}
}