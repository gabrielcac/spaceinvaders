﻿using System;

namespace PandB.Variables
{
	/// <summary>
	/// Allows the use of a <see cref=StringVariable"/> or a constant string.
	/// </summary>
	[Serializable]
	public class StringReference : VariableReference<string, StringVariable>
	{
		#region VariableReference implementation
		public StringReference(string value) : base(value)
		{
		}
		#endregion
	}
}