﻿using System;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// Allows the use of an <see cref=AnimationCurveVariable"/> or a constant <see cref="AnimationCurve"/>.
	/// </summary>
	[Serializable]
	public class AnimationCurveReference : VariableReference<AnimationCurve, AnimationCurveVariable>
	{
		#region VariableReference implementation
		public AnimationCurveReference(AnimationCurve value) : base(value)
		{
		}
		#endregion
	}
}