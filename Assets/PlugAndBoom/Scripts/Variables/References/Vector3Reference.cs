﻿using System;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// Allows the use of a <see cref=Vector3Variable"/> or a constant <see cref="Vector3"/>.
	/// </summary>
	[Serializable]
	public class Vector3Reference : VariableReference<Vector3, Vector3Variable>
	{
		#region VariableReference implementation
		public Vector3Reference(Vector3 value) : base(value)
		{
		}
		#endregion
	}
}