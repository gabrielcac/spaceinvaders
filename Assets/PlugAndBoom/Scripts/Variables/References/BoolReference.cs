﻿using System;

namespace PandB.Variables
{
	/// <summary>
	/// Allows the use of a <see cref=BoolVariable"/> or a constant bool.
	/// </summary>
	[Serializable]
	public class BoolReference : VariableReference<bool, BoolVariable>
	{
		#region VariableReference implementation
		public BoolReference(bool value) : base(value)
		{
		}
		#endregion
	}
}