﻿using System;
using UnityEngine;

namespace PandB.Variables
{
	/// <summary>
	/// Allows the use of a <see cref=Vector2Variable"/> or a constant <see cref="Vector2"/>.
	/// </summary>
	[Serializable]
	public class Vector2Reference : VariableReference<Vector2, Vector2Variable>
	{
		#region VariableReference implementation
		public Vector2Reference(Vector2 value) : base(value)
		{
		}
		#endregion
	}
}