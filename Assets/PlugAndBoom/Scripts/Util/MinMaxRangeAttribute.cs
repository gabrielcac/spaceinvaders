﻿using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Property to be used with <see cref="MinMaxRange"/> to exhibit the min max range object using sliders.
	/// </summary>
	public class MinMaxRangeAttribute : PropertyAttribute
	{
		#region public variables
		public float minLimit;
		public float maxLimit;
		#endregion

		#region public functions
		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="minLimit"></param>
		/// <param name="maxLimit"></param>
		public MinMaxRangeAttribute(float minLimit, float maxLimit)
		{
			this.minLimit = minLimit;
			this.maxLimit = maxLimit;
		}
		#endregion
	}
}