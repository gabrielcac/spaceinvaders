﻿using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Keeps track of the time provided by an external server.
	/// </summary>
	public sealed class ServerTimer : MonoBehaviour
	{
		#region events
		public delegate void ConnectedToTimeServer();
		public static event ConnectedToTimeServer OnConnectedToTimeServer;
		#endregion

		#region private static attributes
		private static ServerTimer _instance;
		#endregion

		#region public attributes
		public int connectionAttempts = 3;
		#endregion

		#region private attributes
		private Thread _connectionThread;
		private object _threadLock = new System.Object();
		private bool _valid;
		private double _serverTime;
		private double _localTimeOnRead;
		#endregion

		#region public static properties
		/// <summary>
		/// Gets if the server timer is online.
		/// </summary>
		public static bool Online
		{
			get
			{
				if(_instance == null)
				{
					return false;
				}
				return _instance._valid;
			}
		}
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			if (_instance == null)
			{
				_instance = this;
				DontDestroyOnLoad(gameObject);
				_serverTime = ClientTime();
				ConnectToServer();
			}
			else
			{
				Destroy(gameObject);
			}
		}
		#endregion

		#region public static functions
		/// <summary>
		/// Gets the time according to the external server. Returns false if no server was found to check the time.
		/// The time is returned as a double representing how many seconds have elapsed since January 1st 1900 in UTC.
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public static bool GetTime(out double time)
		{
			if (_instance != null)
			{
				lock (_instance._threadLock)
				{
					time = _instance._serverTime + (ClientTime() - _instance._localTimeOnRead);
					if(!_instance._valid)
					{
						_instance.ConnectToServer();
					}
					return _instance._valid;
				}
			}
			else
			{
				Debug.LogWarning("Missing a Server Timer in the scene!");
				time = ClientTime();
				return false;
			}
		}

		/// <summary>
		/// Gets the time in a <see cref="DateTime"/> object using the UTC time zone.
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public static bool GetDateTimeUTC(out DateTime time)
		{
			double dTime = 0;
			if (GetTime(out dTime))
			{
				time = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(dTime);
				return true;
			}
			else
			{
				time = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(ClientTime());
				return false;
			}
		}

		/// <summary>
		/// Gets the time ina a <see cref="DateTime"/> object using the local time zone.
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		public static bool GetDateTimeLocal(out DateTime time)
		{
			double dTime = 0;
			if (GetTime(out dTime))
			{
				time = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(dTime).ToLocalTime();
				return true;
			}
			else
			{
				time = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(ClientTime()).ToLocalTime();
				return false;
			}
		}
		#endregion

		#region private static functions
		/// <summary>
		/// Gets the time according to the player device.
		/// The time is returned as a double representing how many seconds have elapsed since January 1st 1900 in UTC.
		/// </summary>
		/// <returns></returns>
		private static double ClientTime()
		{
			return DateTime.Now.Subtract(new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
		}

		/// <summary>
		/// Swap the bytes in an ulong to convert it from the format provided by the NTP server to the system format.
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		private static uint SwapEndianness(ulong x)
		{
			return (uint)(((x & 0x000000ff) << 24) +
						   ((x & 0x0000ff00) << 8) +
						   ((x & 0x00ff0000) >> 8) +
						   ((x & 0xff000000) >> 24));
		}
		#endregion

		#region private functions
		/// <summary>
		/// Start the connection to the server on a separate thread.
		/// </summary>
		private void ConnectToServer()
		{
			if (_connectionThread == null || !_connectionThread.IsAlive)
			{
				_connectionThread = new Thread(Connect);
				_connectionThread.IsBackground = true;
				_connectionThread.Start();

				// Stop all coroutines already checking the connection status before starting a new one
				StopAllCoroutines();
				StartCoroutine(CheckConnected());
			}
		}

		/// <summary>
		/// Connect to the time server.
		/// </summary>
		private void Connect()
		{
			int attempt = 0;
			while (attempt < connectionAttempts)
			{
				try
				{
					byte[] ntpData = new byte[48];

					//LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)
					ntpData[0] = 0x1B;

					IPAddress[] addresses = Dns.GetHostEntry("pool.ntp.org").AddressList;
					Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

					socket.Connect(new IPEndPoint(addresses[0], 123));
					socket.ReceiveTimeout = 1000;

					socket.Send(ntpData);
					socket.Receive(ntpData);
					socket.Close();

					//Offset to get to the "Transmit Timestamp" field (time at which the reply 
					//departed the server for the client, in 64-bit timestamp format."
					const byte serverReplyTime = 40;

					//Get the seconds part
					ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

					//Get the seconds fraction
					ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

					//Convert From big-endian to little-endian
					intPart = SwapEndianness(intPart);
					fractPart = SwapEndianness(fractPart);

					ulong milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

					DateTime networkDateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);
					lock (_threadLock)
					{
						_serverTime = networkDateTime.Subtract(new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
						_localTimeOnRead = ClientTime();
						_valid = true;
					}

					// Set the exit condition of the loop
					attempt = connectionAttempts;
				}
				catch (Exception exception)
				{
					Debug.Log("Could not get NTP time on attempt " + (attempt+1) + "/" + connectionAttempts);
					Debug.Log(exception);
					attempt++;
				}
			}
		}

		/// <summary>
		/// Check if the object could connect to the server and raises an event when it happens.
		/// </summary>
		/// <returns></returns>
		private IEnumerator CheckConnected()
		{
			bool connected = false;
			while (!connected)
			{
				lock (_threadLock)
				{
					connected = _valid;
				}
				yield return null;
			}
			if(OnConnectedToTimeServer != null)
			{
				OnConnectedToTimeServer();
			}
		}
		#endregion
	}
}