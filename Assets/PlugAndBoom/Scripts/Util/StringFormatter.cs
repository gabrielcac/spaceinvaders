﻿using UnityEngine;

namespace PandB
{
	public class StringFormatter
	{
		#region public static functions
		public static string FloatToTime(float value)
		{
			float minutes = Mathf.Floor(value / 60);
			int seconds = Mathf.RoundToInt(value % 60);
			if (seconds < 10f)
			{
				return minutes + ":0" + seconds;
			}
			else
			{
				return minutes + ":" + seconds;
			}
		}

		public static string FloatToInt(float value)
		{
			return ((int)value).ToString();
		}
		#endregion
	}
}