﻿using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Holds two values and gets a random number between them when needed.
	/// </summary>
	[System.Serializable]
	public class MinMaxIntRange
	{
		#region public variables
		public int rangeStart;
		public int rangeEnd;
		#endregion

		#region public functions
		/// <summary>
		/// Gets a random value between <see cref="rangeStart"/> [inclusive] and <see cref="rangeEnd"/> [inclusive].
		/// </summary>
		/// <returns></returns>
		public int GetRandomValue()
		{
			return Random.Range(rangeStart, rangeEnd + 1);
		}
		#endregion
	}
}