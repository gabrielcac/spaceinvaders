﻿using UnityEngine;
using UnityEditor;

namespace PandB
{
	/// <summary>
	/// Draws a <see cref="MinMaxRangeAttribute"/>.
	/// </summary>
	[CustomPropertyDrawer(typeof(MinMaxRangeAttribute))]
	public class MinMaxRangeDrawer : PropertyDrawer
	{
		#region PropertyDrawer implementation
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			// If this is not of the proper type, ignore it and display an error message.
			if (property.type == "MinMaxRange")
			{
				MinMaxRangeAttribute range = attribute as MinMaxRangeAttribute;
				SerializedProperty minValue = property.FindPropertyRelative("rangeStart");
				SerializedProperty maxValue = property.FindPropertyRelative("rangeEnd");
				float newMin = minValue.floatValue;
				float newMax = maxValue.floatValue;
				
				// Place the label and gets the retangle in front of it where the content will be placed
				position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

				// Don't make child fields be indented
				var indent = EditorGUI.indentLevel;
				EditorGUI.indentLevel = 0;

				// Divide the remaining space in 6 parts, so the slider has 4/6 of it, and each field 1/6
				float xDivision = position.width / 6f;

				// Draws the slider
				EditorGUI.MinMaxSlider(new Rect(position.x, position.y, position.width - 2 * xDivision, position.height), 
					ref newMin, ref newMax, range.minLimit, range.maxLimit);

				// Draws fields with the minimum and maximum values selected in the slider
				newMin = Mathf.Clamp(EditorGUI.DelayedFloatField(new Rect(position.x + 4 * xDivision + 4f, position.y, xDivision - 4f, position.height), 
					newMin), range.minLimit, range.maxLimit);
				if(newMin > newMax)
				{
					newMax = newMin;
				}
				newMax = Mathf.Clamp(EditorGUI.DelayedFloatField(new Rect(position.x + 5 * xDivision + 4f, position.y, xDivision - 4f, position.height), 
					newMax), range.minLimit, range.maxLimit);
				if(newMax < newMin)
				{
					newMin = newMax;
				}

				minValue.floatValue = newMin;
				maxValue.floatValue = newMax;

				// Set indent back to what it was
				EditorGUI.indentLevel = indent;
			}
			else if (property.type == "MinMaxIntRange")
			{
				MinMaxRangeAttribute range = attribute as MinMaxRangeAttribute;
				SerializedProperty minValue = property.FindPropertyRelative("rangeStart");
				SerializedProperty maxValue = property.FindPropertyRelative("rangeEnd");
				float newMin = (float)minValue.intValue;
				float newMax = (float)maxValue.intValue;

				// Place the label and gets the retangle in front of it where the content will be placed
				position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

				// Don't make child fields be indented
				var indent = EditorGUI.indentLevel;
				EditorGUI.indentLevel = 0;

				// Divide the remaining space in 6 parts, so the slider has 4/6 of it, and each field 1/6
				float xDivision = position.width / 6f;

				// Draws the slider
				EditorGUI.MinMaxSlider(new Rect(position.x, position.y, position.width - 2 * xDivision, position.height),
					ref newMin, ref newMax, range.minLimit, range.maxLimit);

				// Draws fields with the minimum and maximum values selected in the slider
				newMin = Mathf.Clamp(EditorGUI.DelayedIntField(new Rect(position.x + 4 * xDivision + 4f, position.y, xDivision - 4f, position.height),
					(int)newMin), range.minLimit, range.maxLimit);
				if (newMin > newMax)
				{
					newMax = newMin;
				}
				newMax = Mathf.Clamp(EditorGUI.DelayedIntField(new Rect(position.x + 5 * xDivision + 4f, position.y, xDivision - 4f, position.height),
					(int)newMax), range.minLimit, range.maxLimit);
				if (newMax < newMin)
				{
					newMin = newMax;
				}

				minValue.intValue = (int)newMin;
				maxValue.intValue = (int)newMax;

				// Set indent back to what it was
				EditorGUI.indentLevel = indent;
			}
			else
			{
				Debug.LogWarning("Use only with MinMaxRange type");
			}

			EditorGUI.EndProperty();
		}
		#endregion
	}
}