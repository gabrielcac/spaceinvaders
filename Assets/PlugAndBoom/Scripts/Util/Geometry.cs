﻿using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Utility functions used in many classes.
	/// </summary>
	public class Geometry
	{
		#region public static functions
		/// <summary>
		/// Calculate the bounding box of a <see cref="GameObject"/>.
		/// </summary>
		/// <param name="gameObject"></param>
		/// <returns></returns>
		public static Bounds CalculateBounds(GameObject gameObject)
		{
			Bounds bounds = new Bounds();

			Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
			for(int i = 0, j = renderers.Length; i < j; i++)
			{
				bounds.Encapsulate(renderers[i].bounds);
			}

			return bounds;
		}

		/// <summary>
		/// Calculate the magnitude of a vector in a custom axis.
		/// </summary>
		/// <param name="vector"></param>
		/// <param name="axis"></param>
		/// <returns></returns>
		public static float GetMagnitudeOnCustomAxis(Vector3 vector, Vector3 axis)
		{
			float vectorMag = vector.magnitude;
			float angle = Vector3.Angle(vector, axis);
			float magnitude = 0;
			if (angle == 0)
			{
				magnitude = vector.magnitude;
			}
			else if (angle < 90)
			{
				magnitude = Mathf.Sqrt(Mathf.Pow(vectorMag, 2) - Mathf.Pow(vectorMag * Mathf.Sin(Mathf.Deg2Rad * angle), 2));
			}
			else if (angle == 180)
			{
				magnitude = -vector.magnitude;
			}
			else
			{
				// Because Vector3.angle aways returns the smallest angle, it will never be more than 180
				magnitude = -Mathf.Sqrt(Mathf.Pow(vectorMag, 2) - Mathf.Pow(vectorMag * Mathf.Sin(Mathf.Deg2Rad * (180 - angle)), 2));
			}

			return magnitude;
		}
		#endregion
	}
}