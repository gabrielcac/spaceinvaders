﻿using UnityEngine;

namespace PandB
{
	/// <summary>
	/// Holds two values and gets a random number between them when needed.
	/// </summary>
	[System.Serializable]
	public class MinMaxRange
	{
		#region public variables
		public float rangeStart;
		public float rangeEnd;
		#endregion

		#region public functions
		/// <summary>
		/// Gets a random value between <see cref="rangeStart"/> [inclusive] and <see cref="rangeEnd"/> [inclusive].
		/// </summary>
		/// <returns></returns>
		public float GetRandomValue()
		{
			return Random.Range(rangeStart, rangeEnd);
		}
		#endregion
	}
}