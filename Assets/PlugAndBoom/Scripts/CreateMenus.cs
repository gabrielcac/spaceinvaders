﻿namespace PandB
{
	public sealed class CreateMenus
    {
        public const string dictionaryFileName = "NewDictionary";
        public const string dictionaryMenu = "Plug and Boom/I18NDictionary";
        public const int dictionaryOrder = 40;

        public const string floatVariableFileName = "NewFloat";
		public const string floatVariableMenu = "Plug and Boom/Variable/Float";
		public const int floatVariableOrder = 50;

		public const string intVariableFileName = "NewInt";
		public const string intVariableMenu = "Plug and Boom/Variable/Int";
		public const int intVariableOrder = 51;

		public const string boolVariableFileName = "NewBool";
		public const string boolVariableMenu = "Plug and Boom/Variable/Bool";
		public const int boolVariableOrder = 52;
		
		public const string stringVariableFileName = "NewString";
		public const string stringVariableMenu = "Plug and Boom/Variable/String";
		public const int stringVariableOrder = 53;

		public const string vector3VariableFileName = "NewVector3";
		public const string vector3VariableMenu = "Plug and Boom/Variable/Vector3";
		public const int vector3VariableOrder = 54;

		public const string vector2VariableFileName = "NewVector2";
		public const string vector2VariableMenu = "Plug and Boom/Variable/Vector2";
		public const int vector2VariableOrder = 55;

		public const string colorVariableFileName = "NewColor";
		public const string colorVariableMenu = "Plug and Boom/Variable/Color";
		public const int colorVariableOrder = 56;

		public const string curveVariableFileName = "NewCurve";
		public const string curveVariableMenu = "Plug and Boom/Variable/Animation Curve";
		public const int curveVariableOrder = 57;

		public const string gameEventFileName = "NewEvent";
		public const string gameEventMenu = "Plug and Boom/Event";
		public const int gameEventOrder = 60;

		public const string gameListFileName = "NewGameList";
		public const string gameListMenu = "Plug and Boom/Sets/List";
		public const int gameListOrder = 70;
	}
}