using UnityEngine;
using UnityEditor;

namespace PandB
{
	/// <summary>
	/// Ads a custom menu for Plug & Boom Projects.
	/// </summary>
	public class MenuItems : MonoBehaviour
	{
		public const string rootMenu = "Plug and Boom/";

		public const string builtInResourcesWindow = rootMenu + "Built-in styles and icons";
		public const int builtInResourcesWindowPriority = 10000;

		[MenuItem(rootMenu + "PlayerPrefs/Clear All", false, 10100)]
		private static void NewMenuOption()
		{
			PlayerPrefs.DeleteAll();
			PlayerPrefs.Save();
			Debug.Log ("PlayerPrefs cleaned!");
		}
	}
}