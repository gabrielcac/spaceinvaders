using UnityEngine;
using System.Collections.Generic;
using UnityEngine.VFX;

namespace PandB
{
	/// <summary>
	/// <para>Pause or unpause the game.</para>
	/// <para>The script ignores objects with tags in the ignoreTagsList or objects in any of the layer enumerated
	/// in the ignoreLayersList.</para>
	/// <para>Other scripts are not paused, to avoid compatibility problems with some Unity plugins. Because of that,
	/// game scripts that have to be paused should implement the IPausable interface.</para>
	/// </summary>
	public sealed class Pauser : MonoBehaviour
	{
		#region public variables
		public List<string> ignoreTagsList;
		public List<int> ignoreLayersList;
		#endregion

		#region private variables
		private static Pauser _instance;
		private bool _paused;
		private List<IPausable> paused_objects_list;
		#endregion

		#region public properties
		/// <summary>
		/// Gets the instance of this class, since this class uses the singleton pattern.
		/// </summary>
		/// <value>The instance.</value>
		public static Pauser Instance
		{
			get
			{
				return _instance;
			}
		}

		/// <summary>
		/// Gets a value indicating whether the game is paused.
		/// </summary>
		/// <value><c>true</c> if paused; otherwise, <c>false</c>.</value>
		public bool Paused
		{
			get
			{
				return _paused;
			}
		}
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			// Check if this is the only Pauser on the scene, otherwise destroy this object
			if (_instance == null)
			{
				_instance = this;
			}
			else
			{
				if (this != _instance)
				{
					Destroy(gameObject);
				}
			}
		}

		private void OnDestroy()
		{
			if (this == _instance)
			{
				_instance = null;
			}
		}
		#endregion

		#region public functions
		/// <summary>
		/// <para>Pause all rigid bodies, particle systems, animations, animators, audio sources and itween animations 
		/// currently playing.</para>
		/// <para>Any of these that belongs to a gameObject with a tag on the ignoreTagsList is not paused.</para>
		/// </summary>
		public void Pause()
		{
			if (Paused)
			{
				Debug.LogError("Impossible to pause, the game is already paused!");
				return;
			}

			paused_objects_list = new List<IPausable>();

			// Pause all IPausable scripts
			MonoBehaviour[] scripts = FindObjectsOfType<MonoBehaviour>();
			for (int i = 0; i < scripts.Length; i++)
			{
				// If it is a Monobehaviour, check the tag and layer of the gameObject, otherwise just pause it.
				if (scripts[i] is IPausable)
				{
					if (!ignoreTagsList.Contains((scripts[i]).tag)
						&& !ignoreLayersList.Contains((scripts[i]).gameObject.layer))
					{
						(scripts[i] as IPausable).Pause();
						paused_objects_list.Add(scripts[i] as IPausable);
					}
				}
			}

			// Pause all rigidbodies
			Rigidbody[] rigidbodies = FindObjectsOfType<Rigidbody>();
			for (int i = 0; i < rigidbodies.Length; i++)
			{
				if (!ignoreTagsList.Contains(rigidbodies[i].tag)
				   && !ignoreLayersList.Contains(rigidbodies[i].gameObject.layer))
				{
					RigidbodyData objData = new RigidbodyData(rigidbodies[i]);
					objData.Pause();
					paused_objects_list.Add(objData);
				}
			}

			// Pause all rigidbodies2D
			Rigidbody2D[] rigidBodies2D = FindObjectsOfType<Rigidbody2D>();
			for (int i = 0; i < rigidBodies2D.Length; i++)
			{
				if (!ignoreTagsList.Contains(rigidBodies2D[i].tag)
				   && !ignoreLayersList.Contains(rigidBodies2D[i].gameObject.layer))
				{
					Rigidbody2DData objData = new Rigidbody2DData(rigidBodies2D[i]);
					objData.Pause();
					paused_objects_list.Add(objData);
				}
			}

			// Pause all particle systems
			ParticleSystem[] particleSystems = FindObjectsOfType<ParticleSystem>();
			for (int i = 0; i < particleSystems.Length; i++)
			{
				if (!ignoreTagsList.Contains(particleSystems[i].tag)
				   && !ignoreLayersList.Contains(particleSystems[i].gameObject.layer))
				{
					ParticleSystemData objData = new ParticleSystemData(particleSystems[i]);
					objData.Pause();
					paused_objects_list.Add(objData);
				}
			}

			// Pause all Visual Effects
			VisualEffect[] visualEffects = FindObjectsOfType<VisualEffect>();
			for (int i = 0; i < visualEffects.Length; i++)
			{
				if (!ignoreTagsList.Contains(visualEffects[i].tag)
				   && !ignoreLayersList.Contains(visualEffects[i].gameObject.layer))
				{
					VisualEffectData objData = new VisualEffectData(visualEffects[i]);
					objData.Pause();
					paused_objects_list.Add(objData);
				}
			}

			// Pause all animations
			Animation[] animations = FindObjectsOfType<Animation>();
			for (int i = 0; i < animations.Length; i++)
			{
				if (!ignoreTagsList.Contains(animations[i].tag)
				   && !ignoreLayersList.Contains(animations[i].gameObject.layer))
				{
					AnimationData objData = new AnimationData(animations[i]);
					objData.Pause();
					paused_objects_list.Add(objData);
				}
			}

			// Pause all animators
			Animator[] animators = FindObjectsOfType<Animator>();
			for (int i = 0; i < animators.Length; i++)
			{
				if (!ignoreTagsList.Contains(animators[i].tag)
				   && !ignoreLayersList.Contains(animators[i].gameObject.layer))
				{
					AnimatorData objData = new AnimatorData(animators[i]);
					objData.Pause();
					paused_objects_list.Add(objData);
				}
			}

			// Pause all audio
			AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
			for (int i = 0; i < audioSources.Length; i++)
			{
				if (!ignoreTagsList.Contains(audioSources[i].tag)
				   && !ignoreLayersList.Contains(audioSources[i].gameObject.layer))
				{
					AudioSourceData objData = new AudioSourceData(audioSources[i]);
					objData.Pause();
					paused_objects_list.Add(objData);
				}
			}

			_paused = true;
		}

		/// <summary>
		/// Unpause this game, releasing all paused object to resume their actions.
		/// </summary>
		public void Resume()
		{
			if (!Paused)
			{
				Debug.LogError("Impossible to resume, the game is already running!");
				return;
			}

			for (int i = 0; i < paused_objects_list.Count; i++)
			{
				paused_objects_list[i].Resume();
			}
			_paused = false;
		}
		#endregion

		#region private classes
		/// <summary>
		/// Stores data about all paused AudioSources.
		/// </summary>
		private sealed class AudioSourceData : IPausable
		{
			#region private attributes
			private AudioSource _audio_source;
			private bool _enabled;
			#endregion

			#region public functions
			public AudioSourceData(AudioSource audio_source)
			{
				_audio_source = audio_source;
			}
			#endregion

			#region IPausable implementation
			public void Pause()
			{
				_enabled = _audio_source.isPlaying;
				if (_enabled)
				{
					_audio_source.Pause();
				}
			}

			public void Resume()
			{
				if (_enabled && _audio_source != null)
				{
					_audio_source.Play();
				}
			}
			#endregion
		}

		/// <summary>
		/// Stores data from paused animators.
		/// </summary>
		private sealed class AnimatorData : IPausable
		{
			#region private attributes
			private Animator _animator;
			private bool _enabled;
			#endregion

			#region public functions
			public AnimatorData(Animator animator)
			{
				_animator = animator;
			}
			#endregion

			#region IPausable implementation
			public void Pause()
			{
				_enabled = _animator.enabled;
				if (_enabled)
				{
					_animator.enabled = false;
				}
			}

			public void Resume()
			{
				if (_enabled && _animator != null)
				{
					_animator.enabled = true;
				}
			}
			#endregion
		}

		/// <summary>
		/// Stores data from paused animations.
		/// </summary>
		private sealed class AnimationData : IPausable
		{
			#region private attributes
			private Animation _animation;
			private bool _enabled;
			#endregion

			#region public functions
			public AnimationData(Animation animation)
			{
				_animation = animation;
			}
			#endregion

			#region IPausable implementation
			public void Pause()
			{
				_enabled = _animation.enabled;
				if (_enabled)
				{
					_animation.enabled = false;
				}
			}

			public void Resume()
			{
				if (_enabled && _animation != null)
				{
					_animation.enabled = true;
				}
			}
			#endregion
		}

		/// <summary>
		/// Stores data about paused particle systems.
		/// </summary>
		private sealed class ParticleSystemData : IPausable
		{
			#region private attributes
			private ParticleSystem _particleSystem;
			private bool _enabled;
			#endregion

			#region public functions
			public ParticleSystemData(ParticleSystem particle_system)
			{
				_particleSystem = particle_system;
			}
			#endregion

			#region IPausable implementation
			public void Pause()
			{
				_enabled = _particleSystem.isPlaying;
				if (_enabled)
				{
					_particleSystem.Pause();
				}
			}

			public void Resume()
			{
				if (_enabled)
				{
					_particleSystem.Play();
				}
			}
			#endregion
		}

		/// <summary>
		/// Stores data about paused <see cref="VisualEffect"/>s.
		/// </summary>
		private sealed class VisualEffectData : IPausable
		{
			#region private attributes
			private VisualEffect _visualEffect;
			private bool _enabled;
			#endregion

			#region public functions
			public VisualEffectData(VisualEffect visualEffect)
			{
				_visualEffect = visualEffect;
			}
			#endregion

			#region IPausable implementation
			public void Pause()
			{
				_enabled = _visualEffect.enabled;
				if (_enabled)
				{
					_visualEffect.pause = true;
				}
			}

			public void Resume()
			{
				if (_enabled)
				{
					_visualEffect.pause = false;
				}
			}
			#endregion
		}

		/// <summary>
		/// Stores data about paused 2D rigidbodies.
		/// </summary>
		private sealed class Rigidbody2DData : IPausable
		{
			#region private attributes
			private Rigidbody2D _rigidbody2D;
			private bool _isKinematic;
			private Vector2 _velocity;
			private float _angularVelocity;
			#endregion

			#region public functions
			public Rigidbody2DData(Rigidbody2D rigidbody2D)
			{
				_rigidbody2D = rigidbody2D;
			}
			#endregion

			#region IPausable implementation
			public void Pause()
			{
				_isKinematic = _rigidbody2D.isKinematic;
				_velocity = _rigidbody2D.velocity;
				_angularVelocity = _rigidbody2D.angularVelocity;

				_rigidbody2D.isKinematic = true;
				_rigidbody2D.velocity = Vector2.zero;
				_rigidbody2D.angularVelocity = 0.0f;
			}

			public void Resume()
			{
				if (!_isKinematic)
				{
					if (_rigidbody2D != null)
					{
						_rigidbody2D.isKinematic = _isKinematic;
						_rigidbody2D.velocity = _velocity;
						_rigidbody2D.angularVelocity = _angularVelocity;
					}
				}
			}
			#endregion
		}

		/// <summary>
		/// Stores data about paused Rigidbodies.
		/// </summary>
		private sealed class RigidbodyData : IPausable
		{
			#region private attributes
			private Rigidbody _rigidbody;
			private bool _isKinematic;
			private Vector3 _velocity;
			private Vector3 _angularVelocity;
			#endregion

			#region public functions
			public RigidbodyData(Rigidbody rigidbody)
			{
				_rigidbody = rigidbody;
			}
			#endregion

			#region IPausable implementation
			public void Pause()
			{
				_isKinematic = _rigidbody.isKinematic;
				_velocity = _rigidbody.velocity;
				_angularVelocity = _rigidbody.angularVelocity;

				_rigidbody.isKinematic = true;
				_rigidbody.velocity = Vector3.zero;
				_rigidbody.angularVelocity = Vector3.zero;
			}

			public void Resume()
			{
				if (!_isKinematic)
				{
					if (_rigidbody != null)
					{
						_rigidbody.isKinematic = _isKinematic;
						_rigidbody.velocity = _velocity;
						_rigidbody.angularVelocity = _angularVelocity;
					}
				}
			}
			#endregion
		}
		#endregion
	}
}