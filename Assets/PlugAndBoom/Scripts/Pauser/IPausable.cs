﻿namespace PandB
{
	/// <summary>
	/// Interface for pausable scripts.
	/// </summary>
	public interface IPausable
	{
		/// <summary>
		/// Called by the Pauser class when the game is paused.
		/// </summary>
		void Pause();

		/// <summary>
		/// Called by the Pauser class when the game is resumed.
		/// </summary>
		void Resume();
	}
}