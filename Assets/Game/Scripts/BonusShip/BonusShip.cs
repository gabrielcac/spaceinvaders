﻿using DG.Tweening;
using PandB;
using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// A ship that moves at the top of the screen, and if hit gives the player an extra score.
	/// </summary>
	[RequireComponent(typeof(Rigidbody2D))]
	public sealed class BonusShip : MonoBehaviour
	{
		#region private atributes
		[SerializeField]
		private FloatReference _movementTime = new FloatReference(2f);
		[SerializeField]
		private Ease _movementEase = Ease.Linear;
		[SerializeField]
		[MinMaxRange(-350, 350)]
		private MinMaxRange _movementLimits = new MinMaxRange();

		private Rigidbody2D _rigidbody;
		private Tween _movement;
		#endregion

		#region public properties
		/// <summary>
		/// Gets if the ship is moving.
		/// </summary>
		public bool IsMoving
		{
			get
			{
				return _movement != null;
			}
		}
		#endregion

		#region Monobehaviour
		private void Awake()
		{
			_rigidbody = GetComponent<Rigidbody2D>();
		}

		private void OnDisable()
		{
			Stop();
		}

		private void OnDrawGizmosSelected()
		{
			float yPos = transform.position.y;
			float zPos = transform.position.z;

			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeStart, yPos, zPos), new Vector3(_movementLimits.rangeEnd, yPos, zPos));
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeStart, yPos - 5, zPos), new Vector3(_movementLimits.rangeStart, yPos + 5, zPos));
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeEnd, yPos - 5, zPos), new Vector3(_movementLimits.rangeEnd, yPos + 5, zPos));
		}
		#endregion

		#region public functions
		/// <summary>
		/// Places the ship on a random side of the screen.
		/// </summary>
		public void PlaceShip()
		{
			if(Random.value > .5)
			{
				MoveLeft();
			}
			else
			{
				MoveRight();
			}
		}

		/// <summary>
		/// Pauses the ship movement.
		/// </summary>
		public void PauseMovement()
		{
			if(_movement != null)
			{
				_movement.Pause();
			}
		}

		/// <summary>
		/// Resume the ship movement.
		/// </summary>
		public void ResumeMovement()
		{
			if(_movement != null)
			{
				_movement.Play();
			}
		}

		/// <summary>
		/// Stops the ship.
		/// </summary>
		public void Stop()
		{
			if (_movement != null)
			{
				_movement.Kill(true);
			}
		}
		#endregion

		#region private functions
		/// <summary>
		/// Moves the ship from the right side of the screen to the left.
		/// </summary>
		private void MoveLeft()
		{
			float yPos = transform.position.y;
			Move(new Vector2(_movementLimits.rangeEnd, yPos), new Vector2(_movementLimits.rangeStart, yPos));
		}

		/// <summary>
		/// Moves the ship from the left side of the screen to the right.
		/// </summary>
		private void MoveRight()
		{
			float yPos = transform.position.y;
			Move(new Vector2(_movementLimits.rangeStart, yPos), new Vector2(_movementLimits.rangeEnd, yPos));
		}

		/// <summary>
		/// Places the ship at the origin position and moves it to the destination.
		/// </summary>
		/// <param name="origin"></param>
		/// <param name="destination"></param>
		private void Move(Vector2 origin, Vector2 destination)
		{
			Stop();
			gameObject.SetActive(true);
			transform.position = origin;
			_rigidbody.position = origin;
			_movement = DOTween.To(() => _rigidbody.position, x => _rigidbody.position = x, destination, _movementTime).
				SetEase(_movementEase).OnComplete(() => { _movement = null; gameObject.SetActive(false); });
		}
		#endregion
	}
}