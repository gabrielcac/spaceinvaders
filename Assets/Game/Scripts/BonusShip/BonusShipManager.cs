﻿using PandB.Variables;
using System.Collections;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Manages the bonus ship behaviour, turning it on and off as needed by the <see cref="GameController"/>.
	/// </summary>
	public sealed class BonusShipManager : MonoBehaviour
	{
		#region private atributes
		[SerializeField]
		private FloatReference _time = new FloatReference(12f);
		[SerializeField]
		private BonusShip _ship = null;

		private bool _paused;
		private IEnumerator _timerCoroutine;
		#endregion

		#region MonoBehaviour implementation
		private void OnDisable()
		{
			TurnOff();
		}
		#endregion

		#region public functions
		/// <summary>
		/// Allows the bonus ship to showup.
		/// </summary>
		public void TurnOn()
		{
			if (_timerCoroutine != null)
			{
				TurnOff();
			}
			_timerCoroutine = Timer();
			StartCoroutine(_timerCoroutine);
		}

		/// <summary>
		/// Disables the bonus ship mechanic.
		/// </summary>
		public void TurnOff()
		{
			if(_timerCoroutine != null)
			{
				StopCoroutine(_timerCoroutine);
				_timerCoroutine = null;
			}
			_ship.gameObject.SetActive(false);
		}

		/// <summary>
		/// Pauses the ship movement and its placement timer.
		/// </summary>
		public void Pause()
		{
			_paused = true;
			_ship.PauseMovement();
		}

		/// <summary>
		/// Resume the ship movement and placement timer.
		/// </summary>
		public void Resume()
		{
			_paused = false;
			if (_timerCoroutine != null)
			{
				_ship.ResumeMovement();
			}
			else
			{
				TurnOn();
			}
		}
		#endregion

		#region private functions
		/// <summary>
		/// Coroutine that keeps track of how much the game must wait to place a bonus ship.
		/// </summary>
		/// <returns></returns>
		private IEnumerator Timer()
		{
			float timer = _time.Value;
			while(true)
			{
				if(!_paused)
				{
					timer -= Time.deltaTime;
					if (timer <= 0)
					{
						timer = _time;
						_ship.PlaceShip();
					}
				}
				yield return null;
			}
		}
		#endregion
	}
}