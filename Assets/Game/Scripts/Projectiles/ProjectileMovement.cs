﻿using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Moves a projectile.
	/// </summary>
	public sealed class ProjectileMovement : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private Vector2Reference _velocity = new Vector2Reference(new Vector2(0, 360f));
		#endregion

		#region MonoBehaviour implementation
		private void Update()
		{
			transform.position += Time.deltaTime * (Vector3)_velocity.Value;
		}
		#endregion
	}
}