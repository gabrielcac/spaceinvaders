﻿using UnityEngine;
using UnityEngine.Events;

namespace SpaceInvaders
{
	/// <summary>
	/// Handles the projectile collisions.
	/// </summary>
	public sealed class ProjectileCollision : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private Scorer _shooter = null;
		[SerializeField]
		private UnityEvent _onCollision = null;
		#endregion

		#region MonoBehaviour implementation
		private void OnTriggerEnter2D(Collider2D collision)
		{
			Hitable hitable = collision.GetComponentInParent<Hitable>();
			if (hitable != null)
			{
				hitable.Hit(gameObject, _shooter);
			}
			_onCollision.Invoke();
		}
		#endregion
	}
}