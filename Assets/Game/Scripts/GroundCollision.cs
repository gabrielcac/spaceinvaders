﻿using UnityEngine;
using UnityEngine.Events;

namespace SpaceInvaders
{
	/// <summary>
	/// Gets if an enemy ship landed, and raises an event to end the game.
	/// </summary>
	public sealed class GroundCollision : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private UnityEvent _onEnemyLanded = null;
		#endregion

		#region MonoBehaviour implementation
		private void OnTriggerEnter2D(Collider2D collision)
		{
			if(collision.GetComponentInParent<Enemy>() != null)
			{
				_onEnemyLanded.Invoke();
			}
		}
		#endregion
	}
}