﻿using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// An object that can take hits.
	/// </summary>
	public sealed class Hitable : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private BoolReference _useSourcePosition = new BoolReference(false);
		[SerializeField]
		private Vector3UnityEvent _onHit = null;
		[SerializeField]
		private ScorerUnityEvent _onHit2 = null;
		#endregion

		#region public functions
		/// <summary>
		/// Function caled by a projectile that hits the object.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="shooter"></param>
		public void Hit(GameObject source, Scorer shooter)
		{
			if (_useSourcePosition)
			{
				_onHit.Invoke(source.transform.position);
			}
			else
			{
				_onHit.Invoke(transform.position);
			}

			if(shooter != null)
			{
				_onHit2.Invoke(shooter);
			}
		}
		#endregion
	}
}