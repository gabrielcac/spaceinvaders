﻿using DG.Tweening;
using PandB;
using PandB.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceInvaders
{
	/// <summary>
	/// Moves the boss left or right inside a limit.
	/// </summary>
	public sealed class BossMovement : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private FloatReference _sideStep = null;
		[SerializeField]
		private FloatReference _time = new FloatReference(0);
		[SerializeField]
		private Ease _ease = Ease.Linear;
		[SerializeField]
		[MinMaxRange(-270, 270)]
		private MinMaxRange _movementLimits = new MinMaxRange();
		[SerializeField]
		private UnityEvent _centerReached = null;

		private Tween _tween;
		private bool _movingRight;
		#endregion

		#region MonoBehaviour implementation
		private void OnDisable()
		{
			Stop();
		}

		private void OnDrawGizmosSelected()
		{
			float yPos = transform.position.y;
			float zPos = transform.position.z;

			Gizmos.color = Color.green;
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeStart, yPos, zPos), new Vector3(_movementLimits.rangeEnd, yPos, zPos));
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeStart, yPos - 5, zPos), new Vector3(_movementLimits.rangeStart, yPos + 5, zPos));
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeEnd, yPos - 5, zPos), new Vector3(_movementLimits.rangeEnd, yPos + 5, zPos));
		}
		#endregion

		#region public functions
		/// <summary>
		/// Moves the boss sideways.
		/// </summary>
		public void Move()
		{
			if(_movingRight)
			{
				MoveRight();
			}
			else
			{
				MoveLeft();
			}
		}

		/// <summary>
		/// Moves the enemy right.
		/// </summary>
		public void MoveRight()
		{
			Move(_sideStep * Vector2.right);
		}

		/// <summary>
		/// Moves the enemy left.
		/// </summary>
		public void MoveLeft()
		{
			Move(_sideStep * Vector2.left);
		}
		#endregion

		#region private functions
		/// <summary>
		/// Adds the movement vector to the enemy position.
		/// </summary>
		/// <param name="movement"></param>
		private void Move(Vector2 movement)
		{
			Stop();
			Vector3 movementStart = transform.position;
			_tween = DOTween.To(() => transform.position, x => transform.position = x, transform.position + (Vector3)movement, _time).
				SetEase(_ease).OnComplete(() => { _tween = null; VerifyMovementLimits(movementStart, transform.position); });
		}

		/// <summary>
		/// Stops any movement.
		/// </summary>
		private void Stop()
		{
			if (_tween != null)
			{
				_tween.Kill(true);
			}
		}

		/// <summary>
		/// Verify if the boss reached its moving limits or if it reached the center of teh screen.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		private void VerifyMovementLimits(Vector3 start, Vector3 end)
		{
			if((_movingRight && end.x >= _movementLimits.rangeEnd) ||
				(!_movingRight && end.x <= _movementLimits.rangeStart))
			{
				_movingRight = !_movingRight;
			}
			else if((_movingRight && start.x < 0 && end.x >= 0) ||
				(!_movingRight && start.x > 0 && end.x <= 0))
			{
				_centerReached.Invoke();
			}
		}
		#endregion
	}
}