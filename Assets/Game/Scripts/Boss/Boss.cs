﻿using DG.Tweening;
using PandB.StateMachine;
using PandB.Variables;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// A big bad boss!
	/// Is invulnerable for most of the time, but becomes vulnerable for a few seconds after summoning enemies.
	/// </summary>
	[RequireComponent(typeof(BossMovement))]
	public sealed class Boss : MonoBehaviourStateMachine
	{
		#region private enums
		private enum StateId { Moving, Summoning, Vulnerable, Hit, Dying }
		private enum EventId { ReachedCenter, MovementTick, Hit }
		#endregion

		#region private attributes
		[Header("General Configuration")]
		[SerializeField]
		private IntReference _hitPoints = new IntReference(5);
		//[Header("Moving")]
		[Header("Summoning")]
		[SerializeField]
		private FloatReference _summoningTime = new FloatReference(2f);
		[SerializeField]
		private FloatReference _summoningHeight = new FloatReference(420f);
		[SerializeField]
		private EnemyColumn[] _enemiesColumns = null;
		[SerializeField]
		private Enemy[] _enemyPrefabs = null;
		[Header("Vulnerable")]
		[SerializeField]
		private FloatReference _vulnerableTime = new FloatReference(10f);
		[SerializeField]
		private GameObject _forceField = null;
		[Header("Hit")]
		[SerializeField]
		private FloatReference _shakeDuration = new FloatReference(1f);
		[SerializeField]
		private FloatReference _shakeForce = new FloatReference(90f);
		[Header("Dying")]
		[SerializeField]
		private FloatReference _dyingTime = new FloatReference(3f);
		[Header("Events")]
		[SerializeField]
		private Vector3UnityEvent _onDeath = null;
		[SerializeField]
		private ScorerUnityEvent _onDeath2 = null;

		private BossMovement _movement;
		private List<Enemy> _enemiesUnderControl;
		#endregion

		#region MonoBehaviourStateMachine implementation
		private void Awake()
		{
			_movement = GetComponent<BossMovement>();
			_enemiesUnderControl = new List<Enemy>(22);
		}

		private void Start()
		{
			StartStateMachine();
		}

		protected override void CreateStates(ref Dictionary<int, State> states)
		{
			State state = new Moving(this);
			states.Add(state.Id, state);

			state = new Summoning(this);
			states.Add(state.Id, state);

			state = new Vulnerable(this);
			states.Add(state.Id, state);

			state = new Hit(this);
			states.Add(state.Id, state);

			state = new Dying(this);
			states.Add(state.Id, state);
		}
		#endregion

		#region public functions
		/// <summary>
		/// Called when the <see cref="EnemyTimer"/> ticks.
		/// </summary>
		public void OnTick()
		{
			Event((int)EventId.MovementTick);
		}

		/// <summary>
		/// Called as the boss reaches the center of the screen.
		/// </summary>
		public void OnCenterReached()
		{
			Event((int)EventId.ReachedCenter);
		}

		/// <summary>
		/// Called when the boss takes a hit.
		/// </summary>
		/// <param name="scorer"></param>
		public void OnHit(Scorer scorer)
		{
			Event((int)EventId.Hit, new HitArgs(scorer));
		}
		#endregion

		#region private functions
		private void EnableEnemiesActions(bool enable)
		{
			for(int i = 0, j = _enemiesUnderControl.Count; i < j; i++)
			{
				_enemiesUnderControl[i].GetComponent<EnemyMovement>().AllowMovement(enable);
				_enemiesUnderControl[i].GetComponent<EnemyShooting>().AllowShooting(enable);
			}
		}
		#endregion

		#region private classes
		/// <summary>
		/// While on this state the boss is moving from one side of the screen to the other.
		/// </summary>
		private class Moving : State
		{
			#region private attributes
			private Boss boss;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.Moving;
				}
			}
			#endregion

			#region State implementation
			public Moving(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.Summoning));

				boss = stateMachine as Boss;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
			}

			public override void OnEvent(int eventId, StateMachineArgs args = null)
			{
				EventId newEvent = (EventId)eventId;
				switch(newEvent)
				{
					case EventId.MovementTick:
						boss._movement.Move();
						break;
					case EventId.ReachedCenter:
						StateMachine.ChangeTo((int)StateId.Summoning);
						break;
				}
			}
			#endregion
		}

		/// <summary>
		/// The boss is summoning enemy ships.
		/// </summary>
		private class Summoning : State
		{
			#region private attributes
			private Boss boss;
			private float time;
			private float individualSummonTime;
			private int currentColumn;
			private int totalColumns;
			private Enemy enemyPrefab;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.Summoning;
				}
			}
			#endregion

			#region State implementation
			public Summoning(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.Vulnerable));

				boss = stateMachine as Boss;
				totalColumns = boss._enemiesColumns.Length;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				individualSummonTime = boss._summoningTime / totalColumns;
				time = individualSummonTime;
				currentColumn = 0;
				enemyPrefab = boss._enemyPrefabs[Random.Range(0, boss._enemyPrefabs.Length)];
				boss.EnableEnemiesActions(false);
			}

			public override void Update()
			{
				time -= Time.deltaTime;
				if(time <= 0)
				{
					Enemy enemy = boss._enemiesColumns[currentColumn].AddEnemy(enemyPrefab, boss._summoningHeight);
					boss._enemiesUnderControl.Add(enemy);
					enemy.GetComponent<EnemyMovement>().AllowMovement(false);
					enemy.GetComponent<EnemyShooting>().AllowShooting(false);
					currentColumn += 1;
					if(currentColumn == totalColumns)
					{
						StateMachine.ChangeTo((int)StateId.Vulnerable);
					}
					else
					{
						time = individualSummonTime;
					}
				}
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
				boss.EnableEnemiesActions(true);
			}
			#endregion
		}

		/// <summary>
		/// The boss depleted its energy with the summoning, and its force field is down.
		/// </summary>
		private class Vulnerable : State
		{
			#region private attributes
			private Boss boss;
			private float time;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.Vulnerable;
				}
			}
			#endregion

			#region State implementation
			public Vulnerable(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.Moving));
				AddTransition(new Transition(this, (int)StateId.Hit));
				AddTransition(new Transition(this, (int)StateId.Dying));

				boss = stateMachine as Boss;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				time = boss._vulnerableTime;
				boss._forceField.SetActive(false);
			}

			public override void Update()
			{
				time -= Time.deltaTime;
				if(time <= 0)
				{
					StateMachine.ChangeTo((int)StateId.Moving);
				}
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
				boss._forceField.SetActive(true);
			}

			public override void OnEvent(int eventId, StateMachineArgs args = null)
			{
				EventId newEvent = (EventId)eventId;
				switch (newEvent)
				{
					case EventId.Hit:
						boss._hitPoints.Value -= 1;
						if(boss._hitPoints > 0)
						{
							StateMachine.ChangeTo((int)StateId.Hit, args);
						}
						else
						{
							StateMachine.ChangeTo((int)StateId.Dying, args);
						}
						break;
				}
			}
			#endregion
		}

		/// <summary>
		/// The boss took a hit.
		/// </summary>
		private class Hit : State
		{
			#region private attributes
			private Boss boss;
			private Tween _shake;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.Hit;
				}
			}
			#endregion

			#region State implementation
			public Hit(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.Moving));

				boss = stateMachine as Boss;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				_shake = (StateMachine as MonoBehaviourStateMachine).transform.DOShakeRotation(boss._shakeDuration, new Vector3(0, 0, boss._shakeForce)).
					OnComplete(EndShake);
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
			}

			~Hit()
			{
				if(_shake != null)
				{
					_shake.Kill();
					_shake = null;
				}
			}
			#endregion

			#region private functions
			private void EndShake()
			{
				_shake = null;
				StateMachine.ChangeTo((int)StateId.Moving);
			}
			#endregion
		}

		/// <summary>
		/// The boss is dying.
		/// </summary>
		private class Dying : State
		{
			#region private attributes
			private Boss boss;
			private Tween _shake;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.Dying;
				}
			}
			#endregion

			#region State implementation
			public Dying(IStateMachine stateMachine) : base(stateMachine)
			{
				boss = stateMachine as Boss;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				_shake = (StateMachine as MonoBehaviourStateMachine).transform.DOShakeRotation(boss._dyingTime, new Vector3(0, 0, boss._shakeForce)).
					OnComplete(EndShake);
				boss._onDeath.Invoke(boss.transform.position);
				boss._onDeath2.Invoke((args as HitArgs).Scorer);
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
			}

			~Dying()
			{
				if (_shake != null)
				{
					_shake.Kill();
					_shake = null;
				}
			}
			#endregion

			#region private functions
			private void EndShake()
			{
				_shake = null;
				boss.gameObject.SetActive(false);
			}
			#endregion
		}

		/// <summary>
		/// Sters arguments used when the boss takes a hit.
		/// </summary>
		private class HitArgs : StateMachineArgs
		{
			#region private attributes
			private Scorer _scorer;
			#endregion

			#region public properties
			public Scorer Scorer
			{
				get
				{
					return _scorer;
				}
			}
			#endregion

			#region public functions
			public HitArgs(Scorer scorer)
			{
				_scorer = scorer;
			}
			#endregion
		}
		#endregion
	}
}