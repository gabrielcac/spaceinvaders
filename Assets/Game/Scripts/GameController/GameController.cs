﻿using PandB;
using PandB.StateMachine;
using PandB.Variables;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceInvaders
{
	/// <summary>
	/// State machine that handles the game logic.
	/// </summary>
	public sealed class GameController : MonoBehaviourStateMachine
	{
		#region private enums
		private enum StateId { SettingUpNewGame, LoadingLevel, Playing, Victory, ReplacingPlayer, GameOver }
		private enum EventId { AllEnemiesDead, PlayerDied, AlienLanded, Restart }
		#endregion

		#region private attributes
		[Header("Generic Attributes")]
		[Header("Game Setup")]
		[SerializeField]
		private IntReference _playerLives = null;
		[SerializeField]
		private IntReference _startingLives = new IntReference(3);
		[Header("Level Loading")]
		[SerializeField]
		private LevelLoader _levelLoader = null;
		[SerializeField]
		private IntReference _levelIndex = new IntReference(0);
		[SerializeField]
		private FloatReference _loadingTime = new FloatReference(2f);
		[Header("Playing")]
		[SerializeField]
		private RunTimeList _enemies = null;
		[SerializeField]
		private BonusShipManager _bonusSpaceshipTimer = null;
		//[Header("Victory")]
		//[Header("Game Over")]
		[Header("Events")]
		[SerializeField]
		private UnityEvent _enablePlayer = null;
		[SerializeField]
		private UnityEvent _disablePlayer = null;
		[SerializeField]
		private UnityEvent _showGameOverScreen = null;
		#endregion

		#region MonoBehaviour implementation
		private void Start()
		{
			StartStateMachine();
		}
		#endregion

		#region MonoBehaviourStateMachine implementation
		protected override void CreateStates(ref Dictionary<int, State> states)
		{
			State state = new SettingUpNewGame(this);
			states.Add(state.Id, state);

			state = new LoadingLevel(this);
			states.Add(state.Id, state);

			state = new Playing(this);
			states.Add(state.Id, state);

			state = new ReplacingPlayer(this);
			states.Add(state.Id, state);

			state = new Victory(this);
			states.Add(state.Id, state);

			state = new GameOver(this);
			states.Add(state.Id, state);
		}
		#endregion

		#region public functions
		/// <summary>
		/// Called everytime the list of enemies is updated.
		/// </summary>
		public void OnEnemiesListUpdated()
		{ 
			if(_enemies.Count == 0)
			{
				Event((int)EventId.AllEnemiesDead);
			}
		}

		/// <summary>
		/// Called when the player clicks to restart the game, at the game over screen.
		/// </summary>
		public void OnGameRestartCalled()
		{
			Event((int)EventId.Restart);
		}

		/// <summary>
		/// Called when a player dies.
		/// </summary>
		public void OnPlayerDeath()
		{
			Event((int)EventId.PlayerDied);
		}

		public void OnAlienLanded()
		{
			Event((int)EventId.AlienLanded);
		}
		#endregion

		#region private classes
		/// <summary>
		/// Starting state of the game. Used to get variables to their initial values.
		/// </summary>
		private class SettingUpNewGame : State
		{
			#region private attributes
			private GameController _gameController;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.SettingUpNewGame;
				}
			}
			#endregion

			#region State implementation
			public SettingUpNewGame(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.LoadingLevel));

				_gameController = stateMachine as GameController;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				_gameController._disablePlayer.Invoke();
				_gameController._levelIndex.Value = 0;
				_gameController._playerLives.Value = _gameController._startingLives.Value;
				_gameController._bonusSpaceshipTimer.TurnOff();
				ScoreBoard.ResetScores();
			}

			public override void Update()
			{
				StateMachine.ChangeTo((int)StateId.LoadingLevel);
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
				// Does nothing.
			}
			#endregion
		}

		/// <summary>
		/// Loads a level before the game start.
		/// </summary>
		private class LoadingLevel : State
		{
			#region private attributes
			private GameController _gameController;
			private float _timer;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.LoadingLevel;
				}
			}
			#endregion

			#region State implementation
			public LoadingLevel(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.Playing));

				_gameController = stateMachine as GameController;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				_gameController._levelLoader.UnloadLevel();
				_gameController._levelLoader.LoadLevel(_gameController._levelIndex);
				_gameController._bonusSpaceshipTimer.TurnOff();
				_timer = _gameController._loadingTime;
			}

			public override void Update()
			{
				_timer -= Time.deltaTime;
				if(_timer <= 0)
				{
					StateMachine.ChangeTo((int)StateId.Playing);
				}
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
				// Does nothing.
			}
			#endregion
		}

		/// <summary>
		/// The game is running.
		/// </summary>
		private class Playing : State
		{
			#region private attributes
			private GameController _gameController;
			private IntReference _lives;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.Playing;
				}
			}
			#endregion

			#region State implementation
			public Playing(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.Victory));
				AddTransition(new Transition(this, (int)StateId.GameOver));
				AddTransition(new Transition(this, (int)StateId.ReplacingPlayer));
				AddTransition(new Transition(this, (int)StateId.LoadingLevel));

				_gameController = stateMachine as GameController;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				_gameController._enablePlayer.Invoke();
				_lives = _gameController._playerLives;
				_gameController._bonusSpaceshipTimer.Resume();
			}

			public override void Update()
			{
				if (Input.GetKeyDown(KeyCode.Alpha6))
				{
					_gameController._levelIndex.Value = 5;
					StateMachine.ChangeTo((int)StateId.LoadingLevel);
				}
				else if (Input.GetKeyDown(KeyCode.Alpha5))
				{
					_gameController._levelIndex.Value = 4;
					StateMachine.ChangeTo((int)StateId.LoadingLevel);
				}
				else if (Input.GetKeyDown(KeyCode.Alpha4))
				{
					_gameController._levelIndex.Value = 3;
					StateMachine.ChangeTo((int)StateId.LoadingLevel);
				}
				else if (Input.GetKeyDown(KeyCode.Alpha3))
				{
					_gameController._levelIndex.Value = 2;
					StateMachine.ChangeTo((int)StateId.LoadingLevel);
				}
				else if (Input.GetKeyDown(KeyCode.Alpha2))
				{
					_gameController._levelIndex.Value = 1;
					StateMachine.ChangeTo((int)StateId.LoadingLevel);
				}
				else if (Input.GetKeyDown(KeyCode.Alpha1))
				{
					_gameController._levelIndex.Value = 0;
					StateMachine.ChangeTo((int)StateId.LoadingLevel);
				}
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
				_gameController._disablePlayer.Invoke();
				_gameController._bonusSpaceshipTimer.Pause();
			}

			public override void OnEvent(int eventId, StateMachineArgs args = null)
			{
				EventId newEvent = (EventId)eventId;
				switch (newEvent)
				{
					case EventId.AllEnemiesDead:
						StateMachine.ChangeTo((int)StateId.Victory);
						break;
					case EventId.PlayerDied:
						if (_lives > 1)
						{
							StateMachine.ChangeTo((int)StateId.ReplacingPlayer);
						}
						else
						{
							StateMachine.ChangeTo((int)StateId.GameOver);
						}
						break;
					case EventId.AlienLanded:
						StateMachine.ChangeTo((int)StateId.GameOver);
						break;
				}
			}
			#endregion
		}

		private class ReplacingPlayer : State
		{
			#region private attributes
			private GameController _gameController;
			private float _timer;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.ReplacingPlayer;
				}
			}
			#endregion

			#region State implementation
			public ReplacingPlayer(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.Playing));

				_gameController = stateMachine as GameController;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				_gameController._playerLives.Value -= 1;
				_timer = _gameController._loadingTime;
			}

			public override void Update()
			{
				_timer -= Time.deltaTime;
				if(_timer <= 0)
				{
					StateMachine.ChangeTo((int)StateId.Playing);
				}
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
			}

			public override void OnEvent(int eventId, StateMachineArgs args = null)
			{
			}
			#endregion
		}

		/// <summary>
		/// The player won the game, and a new match will start soon.
		/// </summary>
		private class Victory : State
		{
			#region private attributes
			private GameController _gameController;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.Victory;
				}
			}
			#endregion

			#region State implementation
			public Victory(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.LoadingLevel));

				_gameController = stateMachine as GameController;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				_gameController._levelLoader.UnloadLevel();
				_gameController._levelIndex.Value += 1;
				_gameController._bonusSpaceshipTimer.TurnOff();
			}

			public override void Update()
			{
				StateMachine.ChangeTo((int)StateId.LoadingLevel);
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
			}
			#endregion
		}

		/// <summary>
		/// The player lost.
		/// </summary>
		private class GameOver : State
		{
			#region private attributes
			private GameController _gameController;
			#endregion

			#region public properties
			public override int Id
			{
				get
				{
					return (int)StateId.GameOver;
				}
			}
			#endregion

			#region State implementation
			public GameOver(IStateMachine stateMachine) : base(stateMachine)
			{
				AddTransition(new Transition(this, (int)StateId.SettingUpNewGame));

				_gameController = stateMachine as GameController;
			}

			public override void OnStateEnter(StateMachineArgs args = null)
			{
				_gameController._levelLoader.UnloadLevel();
				_gameController._showGameOverScreen.Invoke();
				_gameController._bonusSpaceshipTimer.TurnOff();
				ScoreBoard.UpdateHighestScore();
			}

			public override void OnStateLeave(StateMachineArgs args = null)
			{
				// TODO: Save the Hi-score.
			}

			public override void OnEvent(int eventId, StateMachineArgs args = null)
			{
				EventId newEvent = (EventId)eventId;
				switch (newEvent)
				{
					case EventId.Restart:
						StateMachine.ChangeTo((int)StateId.SettingUpNewGame);
						break;
				}
			}
			#endregion
		}
		#endregion
	}
}