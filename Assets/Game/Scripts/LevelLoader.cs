﻿using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Loads the enemy formation for the level.
	/// </summary>
	public sealed class LevelLoader : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private GameObject[] _levels = null;
		#endregion

		#region public functions
		/// <summary>
		/// Loads the level enemies.
		/// </summary>
		/// <param name="level"></param>
		public void LoadLevel(int level)
		{
			Instantiate(_levels[level % _levels.Length], transform);
		}
		#endregion

		#region private functions
		/// <summary>
		/// Unloads all enemies.
		/// </summary>
		public void UnloadLevel()
		{
			if (transform.childCount > 0)
			{
				Destroy(transform.GetChild(0).gameObject);
			}
		}
		#endregion
	}
}