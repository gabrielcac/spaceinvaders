﻿namespace SpaceInvaders
{
	public sealed class CreateMenus
	{
		public const string scorerFileName = "New Scorer";
		public const string scorerMenu = "Plug and Boom/Space Invaders/Scorer";
		public const int scorerOrder = 1;
	}
}