﻿using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// A pool of <see cref="MonoBehaviour"/> objects.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public sealed class MonoBehaviourPool<T> where T : MonoBehaviour
	{
		#region public events
		public delegate void ObjectPreloaded(ref T instance);
		public event ObjectPreloaded OnObjectPreloaded;
		#endregion

		#region private attributes
		private Dictionary<string, List<T>> _pool;
		private int _maxPreload;
		#endregion

		#region public functions
		/// <summary>
		/// Instantiate a new pool with a limit on the number of items that can be preloaded.
		/// </summary>
		/// <param name="maxPreload"></param>
		public MonoBehaviourPool(int maxPreload = 10)
		{
			_pool = new Dictionary<string, List<T>>();
			_maxPreload = maxPreload;
		}

		/// <summary>
		/// Instantiate a new object or find an inactive instantiated copy and return it.
		/// If a new object is instantiated, it will be instantiated as a child to the parent parameter.
		/// </summary>
		/// <param name="prefab"></param>
		/// <param name="parent"></param>
		/// <returns></returns>
		public T GetCopy(T prefab, Transform parent = null)
		{
			T instance;
			string key = GetPoolKey(prefab);

			// Sometimes the pool can get dirty, if an external script destroy an object
			// so we need to iterate over the objects in the pool and only return one that still
			// exists.
			if (_pool.ContainsKey(key))
			{
				while (_pool[key].Count > 0)
				{
					instance = _pool[key][0];
					_pool[key].RemoveAt(0);
					if (instance != null)
					{
						return instance;
					}
				}
			}

			prefab.gameObject.SetActive(false);
			instance = Object.Instantiate(prefab, parent);
			prefab.gameObject.SetActive(true);
			return instance;
		}

		/// <summary>
		/// Adds an object back in the pool to be reused.
		/// </summary>
		/// <param name="instance"></param>
		public void AddToPool(T instance)
		{
			if (!_pool.ContainsKey(instance.name))
			{
				_pool.Add(instance.name, new List<T>());
			}
			_pool[instance.name].Add(instance);
		}

		/// <summary>
		/// Removes an object from the pool.
		/// </summary>
		/// <param name="instance"></param>
		public void RemoveFromPool(T instance)
		{
			if (_pool.ContainsKey(instance.name))
			{
				_pool[instance.name].Remove(instance);
			}
		}

		/// <summary>
		/// Preload effects to avoid instantiation at runtime.
		/// </summary>
		/// <param name="prefab"></param>
		/// <param name="quantity"></param>
		/// <param name="parent"></param>
		public void Preload(T prefab, int quantity = 1, Transform parent = null)
		{
			quantity = Mathf.Min(quantity, _maxPreload - GetPoolSize(prefab));
			prefab.gameObject.SetActive(false);
			for (int i = 0; i < quantity; i++)
			{
				T instance = Object.Instantiate(prefab, parent);
				AddToPool(instance);
				if (OnObjectPreloaded != null)
				{
					OnObjectPreloaded(ref instance);
				}
			}
			prefab.gameObject.SetActive(true);
		}
		#endregion

		#region private static functions
		/// <summary>
		/// Gets the string representing an object in the pool.
		/// </summary>
		/// <param name="prefab"></param>
		/// <returns></returns>
		private string GetPoolKey(T prefab)
		{
			return prefab.name + "(Clone)"; ;
		}

		/// <summary>
		/// Gets the number of copies of an object in the pool.
		/// </summary>
		/// <param name="prefab"></param>
		/// <returns></returns>
		private int GetPoolSize(T prefab)
		{
			string key = GetPoolKey(prefab);
			if (_pool.ContainsKey(key))
			{
				return _pool[key].Count;
			}
			return 0;
		}
		#endregion
	}
}