﻿using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Uses a pool to minimize the number of objects and instantiation at runtime.
	/// </summary>
	public sealed class PoolMember : MonoBehaviour
	{
		#region private static attributes
		private static MonoBehaviourPool<PoolMember> _pool;
		#endregion

		#region private static properties
		/// <summary>
		/// Pool with all the instantiated effects used in the game.
		/// </summary>
		private static MonoBehaviourPool<PoolMember> Pool
		{
			get
			{
				if (_pool == null)
				{
					_pool = new MonoBehaviourPool<PoolMember>(10);
				}
				return _pool;
			}
		}
		#endregion

		#region MonoBehaviour implementation
		private void OnDisable()
		{
			Pool.AddToPool(this);
		}

		private void OnDestroy()
		{
			Pool.RemoveFromPool(this);
		}
		#endregion

		#region public static functions
		/// <summary>
		/// Places a copy of the object in the defined position with the defined rotation.
		/// </summary>
		/// <param name="prefab"></param>
		/// <param name="position"></param>
		/// <param name="rotation"></param>
		/// <returns></returns>
		public static PoolMember Place(PoolMember prefab, Vector3 position, Quaternion rotation)
		{
			PoolMember poolMember = Pool.GetCopy(prefab);
			poolMember.transform.position = position;
			if(poolMember.TryGetComponent(out Rigidbody rigidbody))
			{
				rigidbody.position = position;
			}
			if(poolMember.TryGetComponent(out Rigidbody2D rigidbody2D))
			{
				rigidbody2D.position = position;
			}
			poolMember.transform.rotation = rotation;
			poolMember.gameObject.SetActive(true);

			return poolMember;
		}

		/// <summary>
		/// Places a copy of the object in the defined position.
		/// </summary>
		/// <param name="prefab"></param>
		/// <param name="position"></param>
		/// <returns></returns>
		public static PoolMember Place(PoolMember prefab, Vector3 position)
		{
			return Place(prefab, position, Quaternion.identity);
		}

		/// <summary>
		/// Place a copy of the object at a  <see cref="GameObject"/> position.
		/// </summary>
		/// <param name="prefab"></param>
		/// <param name="target"></param>
		/// <returns></returns>
		public static PoolMember Place(PoolMember prefab, GameObject target)
		{
			return Place(prefab, target.transform.position, target.transform.rotation);
		}

		/// <summary>
		/// Preload objects to avoid instantiation at runtime.
		/// </summary>
		/// <param name="prefab"></param>
		/// <param name="quantity"></param>
		public static void Preload(PoolMember prefab, int quantity = 1)
		{
			Pool.Preload(prefab, quantity);
		}
		#endregion

		#region public functions
		/// <summary>
		/// Place a copy of this object at the specified position.
		/// </summary>
		/// <param name="position"></param>
		public void PlaceACopy(Vector3 position)
		{
			Place(this, position);
		}

		/// <summary>
		/// Place a copy of this object at the specified position with the specified rotation.
		/// </summary>
		/// <param name="position"></param>
		/// <param name="rotation"></param>
		public void PlaceACopy(Vector3 position, Quaternion rotation)
		{
			Place(this, position, rotation);
		}

		/// <summary>
		/// Place a copy of this object at a <see cref="GameObject"/> position.
		/// </summary>
		/// <param name="target"></param>
		public void PlaceACopy(GameObject target)
		{
			Place(this, target);
		}

		/// <summary>
		/// Disable the game object.
		/// </summary>
		public void Disable()
		{
			gameObject.SetActive(false);
		}
		#endregion
	}
}