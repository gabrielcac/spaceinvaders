﻿using PandB.Variables;
using TMPro;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Display a score value with at least 5 digits.
	/// </summary>
	[RequireComponent(typeof(TMP_Text))]
	public sealed class ScoreDisplay : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private IntReference _score = null;

		private TMP_Text _text;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_text = GetComponent<TMP_Text>();
		}

		private void OnEnable()
		{
			UpdateDisplay();
		}
		#endregion

		#region public functions
		/// <summary>
		/// Updates the value displayed.
		/// </summary>
		public void UpdateDisplay()
		{
			_text.text = _score.Value.ToString("D5");
		}
		#endregion
	}
}