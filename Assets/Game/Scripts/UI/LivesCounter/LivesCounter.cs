﻿using PandB.Variables;
using TMPro;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Updates the counter of lives in the HUD.
	/// </summary>
	[RequireComponent(typeof(TMP_Text))]
	public sealed class LivesCounter : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private IntReference _livesCounter = null;

		private TMP_Text _text;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_text = GetComponent<TMP_Text>();
		}
		#endregion

		#region public functions
		/// <summary>
		/// Updates the value displayed.
		/// </summary>
		public void UpdateDisplay()
		{
			_text.text = _livesCounter.Value.ToString();
		}
		#endregion
	}
}