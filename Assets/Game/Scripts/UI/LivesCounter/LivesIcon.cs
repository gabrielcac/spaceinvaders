﻿using PandB.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceInvaders
{
	/// <summary>
	/// Updates the extra lives icons in the HUD.
	/// </summary>
	public sealed class LivesIcon : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private IntReference _livesCounter = null;
		[SerializeField]
		private IntReference _displayFor = new IntReference(2);

		private Image _image;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_image = GetComponent<Image>();
		}
		#endregion

		#region public functions
		/// <summary>
		/// Hides or show the icon as needed.
		/// </summary>
		public void UpdateDisplay()
		{
			if(_livesCounter.Value >= _displayFor.Value)
			{
				_image.enabled = true;
			}
			else
			{
				_image.enabled = false;
			}
		}
		#endregion
	}
}