﻿using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Makes enemies collide with the player and obstacles.
	/// </summary>
	public sealed class EnemyCollision : MonoBehaviour
	{
		#region MonoBehaviour implementation
		private void OnTriggerEnter2D(Collider2D collision)
		{
			Hitable hitable = collision.GetComponentInParent<Hitable>();
			if (hitable != null)
			{
				hitable.Hit(gameObject, null);
			}
		}

		private void OnTriggerStay2D(Collider2D collision)
		{
			Hitable hitable = collision.GetComponentInParent<Hitable>();
			if (hitable != null)
			{
				hitable.Hit(gameObject, null);
			}
		}
		#endregion
	}
}