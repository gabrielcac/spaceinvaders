﻿using PandB.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceInvaders
{
	/// <summary>
	/// A timer that ticks at an interval, telling the enemies to move.
	/// </summary>
	public sealed class EnemyTimer : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private FloatReference _tickInterval = new FloatReference(1f);
		[SerializeField]
		private UnityEvent _onTick = null;

		private float _timer;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_timer = _tickInterval;
		}

		private void Update()
		{
			_timer -= Time.deltaTime;
			if(_timer <= 0)
			{
				_timer = _tickInterval;
				_onTick.Invoke();
			}
		}
		#endregion
	}
}