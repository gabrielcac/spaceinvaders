﻿using DG.Tweening;
using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Moves the enemy left, right or down.
	/// </summary>
	public sealed class EnemyMovement : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private FloatReference _sideStep = null;
		[SerializeField]
		private FloatReference _downStep = null;
		[SerializeField]
		private FloatReference _time = new FloatReference(0);
		[SerializeField]
		private Ease _ease = Ease.Linear;

		private Tween _tween;

		private bool _allowMovement;
		#endregion

		#region MonoBehaviour implementation
		private void OnEnable()
		{
			_allowMovement = true;
		}

		private void OnDisable()
		{
			Stop();
		}
		#endregion

		#region public functions
		/// <summary>
		/// Moves the enemy right.
		/// </summary>
		public void MoveRight()
		{
			if (_allowMovement)
			{
				Move(_sideStep * Vector2.right);
			}
		}

		/// <summary>
		/// Moves the enemy left.
		/// </summary>
		public void MoveLeft()
		{
			if (_allowMovement)
			{
				Move(_sideStep * Vector2.left);
			}
		}

		/// <summary>
		/// Moves the enemy down.
		/// </summary>
		public void MoveDown()
		{
			if (_allowMovement)
			{
				Move(_downStep * Vector2.down);
			}
		}

		/// <summary>
		/// Turns the enemy movement on or off.
		/// </summary>
		/// <param name="allow"></param>
		public void AllowMovement(bool allow)
		{
			_allowMovement = allow;
		}
		#endregion

		#region private functions
		/// <summary>
		/// Adds the movement vector to the enemy position.
		/// </summary>
		/// <param name="movement"></param>
		private void Move(Vector2 movement)
		{
			Stop();
			_tween = DOTween.To(() => transform.position, x => transform.position = x, transform.position + (Vector3)movement, _time).
				SetEase(_ease).OnComplete(() => { _tween = null; });
		}

		/// <summary>
		/// Stops any movement.
		/// </summary>
		private void Stop()
		{
			if(_tween != null)
			{
				_tween.Kill(true);
			}
		}
		#endregion
	}
}