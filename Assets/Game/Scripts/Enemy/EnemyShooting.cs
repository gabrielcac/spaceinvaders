﻿using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Allows an enemy to fire a projectile.
	/// </summary>
	public sealed class EnemyShooting : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private Vector3UnityEvent _onFire = null;

		private bool _allowShooting;
		#endregion

		#region MonoBehaviour implementation
		private void OnEnable()
		{
			_allowShooting = true;
		}
		#endregion

		#region public functions
		/// <summary>
		/// Fire a projectile.
		/// </summary>
		public void Shoot()
		{
			if (_allowShooting)
			{
				_onFire.Invoke(transform.position);
			}
		}

		/// <summary>
		/// Enables or disables the abiblity of this enemy to shoot.
		/// </summary>
		/// <param name="allow"></param>
		public void AllowShooting(bool allow)
		{
			_allowShooting = allow;
		}
		#endregion
	}
}