﻿using PandB;
using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Accelerates the movement of the enemies as less enemies remain alive.
	/// </summary>
	public sealed class EnemyMovementAcceleration : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private AnimationCurveReference _movementIntervalCurve = null;
		[SerializeField]
		private FloatReference _intervalOut = null;
		[SerializeField]
		private RunTimeList _enemies = null;
		#endregion

		#region public functions
		/// <summary>
		/// Update the interval between movements.
		/// </summary>
		public void UpdateInterval()
		{
			_intervalOut.Value = _movementIntervalCurve.Value.Evaluate(_enemies.Count);
		}
		#endregion
	}
}