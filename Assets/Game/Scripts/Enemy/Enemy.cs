﻿using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Simplify the assembly of an enemy by adding all the needed components.
	/// </summary>
	[RequireComponent(typeof(Hitable))]
	[RequireComponent(typeof(EnemyMovement))]
	[RequireComponent(typeof(EnemyShooting))]
	[RequireComponent(typeof(ScoreSource))]
	[RequireComponent(typeof(EnemyCollision))]
	[RequireComponent(typeof(PoolMember))]
	public sealed class Enemy : MonoBehaviour
	{
	}
}