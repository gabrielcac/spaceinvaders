﻿using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Controls a column of enemies.
	/// </summary>
	public sealed class EnemyColumn : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private List<Enemy> _enemies = null;
		#endregion

		#region public functions
		/// <summary>
		/// Makes the lower enemy on the column shoot.
		/// </summary>
		public void Shoot()
		{
			Enemy enemy = GetLowerEnemy();
			if (enemy != null)
			{
				enemy.GetComponent<EnemyShooting>().Shoot();
			}
		}

		/// <summary>
		/// Gets if the column has at least one enemy alive.
		/// </summary>
		/// <returns></returns>
		public bool HasEnemies()
		{
			for (int i = _enemies.Count - 1; i >= 0; i--)
			{
				if (_enemies[i].gameObject.activeInHierarchy)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Adds an enemy to the column.
		/// </summary>
		/// <param name="enemyPrefab"></param>
		/// <param name="height"></param>
		public Enemy AddEnemy(Enemy enemyPrefab, float height)
		{
			PoolMember poolMember = enemyPrefab.GetComponent<PoolMember>();
			Enemy enemy = PoolMember.Place(poolMember, new Vector3(transform.position.x, height, transform.position.z)).GetComponent<Enemy>();
			_enemies.Add(enemy);
			return enemy;
		}
		#endregion

		#region private functions
		/// <summary>
		/// Gets the enemy that is lower in the column.
		/// </summary>
		/// <returns></returns>
		private Enemy GetLowerEnemy()
		{
			Enemy _lower = null;
			for(int i = _enemies.Count - 1; i >= 0; i--)
			{
				if(_enemies[i].gameObject.activeInHierarchy)
				{
					if(_lower == null || _enemies[i].transform.position.y < _lower.transform.position.y)
					{
						_lower = _enemies[i];
					}
				}
			}
			return _lower;
		}
		#endregion
	}
}