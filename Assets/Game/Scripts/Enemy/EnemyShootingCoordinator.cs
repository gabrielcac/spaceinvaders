﻿using PandB.Variables;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Coordinate when enemies should shoot down, and picks the enemy to do so.
	/// </summary>
	public sealed class EnemyShootingCoordinator : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private IntReference _ticksToShoot = new IntReference(4);
		[SerializeField]
		private EnemyColumn[] _columns = null;

		private int _ticksCounter;
		private List<EnemyColumn> _availableColumns;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_availableColumns = new List<EnemyColumn>(_columns.Length);
		}
		#endregion

		#region public functions
		/// <summary>
		/// Called everytime the movement timer ticks to check if it is time to shoot a projectile.
		/// </summary>
		public void OnTimerTick()
		{
			_ticksCounter += 1;
			if(_ticksCounter >= _ticksToShoot)
			{
				_ticksCounter = 0;
				GetPopulatedColumns(_availableColumns);
				if (_availableColumns.Count > 0)
				{
					_availableColumns[Random.Range(0, _availableColumns.Count)].Shoot();
				}
			}
		}
		#endregion

		#region private functions
		/// <summary>
		/// Gets a list with columns that still have enemies alive.
		/// </summary>
		/// <param name="columns"></param>
		private void GetPopulatedColumns(List<EnemyColumn> columns)
		{
			columns.Clear();
			for(int i = _columns.Length - 1; i >= 0; i--)
			{
				if(_columns[i].HasEnemies())
				{
					columns.Add(_columns[i]);
				}
			}
		}
		#endregion
	}
}