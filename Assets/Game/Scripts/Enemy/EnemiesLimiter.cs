﻿using PandB;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceInvaders
{
	/// <summary>
	/// Raises an event when an enemy moves past a limit.
	/// </summary>
	public sealed class EnemiesLimiter : MonoBehaviour
	{
		#region public enum
		public enum Side { Left, Right, Down }
		#endregion

		#region private attributes
		[Header("Enemies list")]
		[SerializeField]
		private Side _side = Side.Left;
		[SerializeField]
		private RunTimeList _enemiesList = null;
		[Header("Events")]
		[SerializeField]
		private UnityEvent _onLimitReached = null;
		#endregion

		#region MonoBehaviour implementation
		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.yellow;
			switch (_side)
			{
				case Side.Left:
				case Side.Right:
					Gizmos.DrawLine(new Vector3(transform.position.x, -1000, transform.position.z), new Vector3(transform.position.x, 1000, transform.position.z));
					break;
				case Side.Down:
					Gizmos.DrawLine(new Vector3(-1000, transform.position.y, transform.position.z), new Vector3(1000, transform.position.y, transform.position.z));
					break;
			}
		}
		#endregion

		#region public functions
		/// <summary>
		/// Verify the position of all active enemies.
		/// </summary>
		public void VerifyEnemiesPosition()
		{
			for (int i = _enemiesList.Count - 1; i >= 0; i--)
			{
				if (IsBeyondLimit(_enemiesList[i].transform.position))
				{
					_onLimitReached.Invoke();
					break;
				}
			}
		}
		#endregion

		#region private functions
		/// <summary>
		/// Verify if a position is beyond the limit.
		/// </summary>
		/// <param name="position"></param>
		/// <returns></returns>
		private bool IsBeyondLimit(Vector3 position)
		{
			switch(_side)
			{
				case Side.Left:
					return position.x <= transform.position.x;
				case Side.Right:
					return position.x >= transform.position.x;
				case Side.Down:
					return position.y <= transform.position.y;
				default:
					return false;
			}
		}
		#endregion
	}
}