﻿using PandB;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Keeps track of where the enemies need to move next.
	/// </summary>
	public sealed class EnemyMovementCoordinator : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private RunTimeList _activeEnemies = null;
		[SerializeField]
		private EnemiesLimiter _leftLimit = null;
		[SerializeField]
		private EnemiesLimiter _rightLimit = null;

		private bool _moveLeft = false;
		private bool _moveDown = false;
		#endregion

		#region public functions
		/// <summary>
		/// Prepare the enemies to move down and invert the horizontal movement.
		/// </summary>
		public void InvertMovement()
		{
			_moveLeft = !_moveLeft;
			_moveDown = true;
		}

		/// <summary>
		/// Move all the enemies.
		/// </summary>
		public void MoveEnemies()
		{
			if(!_moveDown)
			{
				if(_moveLeft)
				{
					_leftLimit.VerifyEnemiesPosition();
				}
				else
				{
					_rightLimit.VerifyEnemiesPosition();
				}
			}

			if(_moveDown)
			{
				_moveDown = false;
				for(int i = _activeEnemies.Count - 1; i >= 0; i--)
				{
					if(_activeEnemies[i].TryGetComponent(out EnemyMovement enemyMovement))
					{
						enemyMovement.MoveDown();
					}
				}
			}
			else
			{
				if (_moveLeft)
				{
					for (int i = _activeEnemies.Count - 1; i >= 0; i--)
					{
						if (_activeEnemies[i].TryGetComponent(out EnemyMovement enemyMovement))
						{
							enemyMovement.MoveLeft();
						}
					}
				}
				else
				{
					for (int i = _activeEnemies.Count - 1; i >= 0; i--)
					{
						if (_activeEnemies[i].TryGetComponent(out EnemyMovement enemyMovement))
						{
							enemyMovement.MoveRight();
						}
					}
				}
			}
		}
		#endregion
	}
}