﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceInvaders
{
    /// <summary>
    /// An <see cref="UnityEvent"/> that takes a <see cref="Vector3"/> as a parameter.
    /// </summary>
    [Serializable]
    public sealed class Vector3UnityEvent : UnityEvent<Vector3>
    {
    }
}