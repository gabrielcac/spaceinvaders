﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceInvaders
{
    /// <summary>
    /// An <see cref="UnityEvent"/> that takes a <see cref="Scorer"/> as a parameter.
    /// </summary>
    [Serializable]
    public sealed class ScorerUnityEvent : UnityEvent<Scorer>
    {
    }
}