﻿using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Calls an event after a time.
	/// </summary>
	public sealed class Timer : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private FloatReference _time = new FloatReference(3f);
		[SerializeField]
		private Vector3UnityEvent _onTime = null;

		private float _timer;
		#endregion

		#region MonoBehaviour implementation
		private void OnEnable()
		{
			_timer = _time;
		}

		private void Update()
		{
			if(_timer > 0)
			{
				_timer -= Time.deltaTime;
				if(_timer <= 0)
				{
					_onTime.Invoke(transform.position);
				}
			}
		}
		#endregion
	}
}