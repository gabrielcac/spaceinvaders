﻿using PandB.Variables;
using System;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Keeps track of the score of each player.
	/// </summary>
	public sealed class ScoreBoard : MonoBehaviour
	{
		#region private static attributes
		private static ScoreBoard _instance;
		#endregion

		#region private attributes
		[SerializeField]
		private PlayerScore[] _scores = null;
		[SerializeField]
		private IntReference _highestScore = null;
		[SerializeField]
		private StringReference _highestScoreSaveKey = new StringReference("HighestScore");
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			if(_instance != null)
			{
				Destroy(gameObject);
				return;
			}

			_instance = this;
			LoadHighestScore();
		}

		private void OnDestroy()
		{
			if(_instance == this)
			{
				_instance = null;
			}
		}
		#endregion

		#region public static functions
		/// <summary>
		/// Adds score to a player total score.
		/// </summary>
		/// <param name="scorer"></param>
		/// <param name="score"></param>
		public static void AddScore(Scorer scorer, int score)
		{
			if(_instance == null)
			{
				return;
			}

			_instance.GetPlayerScore(scorer).Score += score;
		}

		/// <summary>
		/// Gets a player total score.
		/// </summary>
		/// <param name="scorer"></param>
		/// <returns></returns>
		public static int GetScore(Scorer scorer)
		{
			if (_instance == null)
			{
				return 0;
			}

			return _instance.GetPlayerScore(scorer).Score;
		}

		/// <summary>
		/// Reset all score counters to 0.
		/// </summary>
		public static void ResetScores()
		{
			if (_instance == null)
			{
				return;
			}

			for (int i = 0, j = _instance._scores.Length; i < j; i++)
			{
				_instance._scores[i].Score = 0;
			}
		}

		/// <summary>
		/// Verify if any of the player scores is greater than the current highest score.
		/// </summary>
		public static void UpdateHighestScore()
		{
			if (_instance == null)
			{
				return;
			}

			int newScore = _instance.GetPlayersHighestScore();
			if(newScore > _instance._highestScore)
			{
				_instance._highestScore.Value = newScore;
				_instance.SaveHighestScore();
			}
		}
		#endregion

		#region private functions
		/// <summary>
		/// Finds a player score.
		/// </summary>
		/// <param name="scorer"></param>
		/// <returns></returns>
		private PlayerScore GetPlayerScore(Scorer scorer)
		{
			for(int i = 0, j = _scores.Length; i < j; i++)
			{
				if (_scores[i].Scorer == scorer)
				{
					return _scores[i];
				}
			}
			return null;
		}

		/// <summary>
		/// Loads the highest score from the player prefs. Since this is the only thing being persisted,
		/// it i simpler to do it this way than to implemment a data layer.
		/// </summary>
		private void LoadHighestScore()
		{
			if(PlayerPrefs.HasKey(_highestScoreSaveKey))
			{
				int highestScore = PlayerPrefs.GetInt(_highestScoreSaveKey);
				_highestScore.Value = highestScore;
			}
		}

		/// <summary>
		/// Saves the highest score to the player prefs.
		/// </summary>
		private void SaveHighestScore()
		{
			PlayerPrefs.SetInt(_highestScoreSaveKey, _highestScore);
			PlayerPrefs.Save();
		}

		/// <summary>
		/// Gets the highest score among the players current score.
		/// </summary>
		/// <returns></returns>
		private int GetPlayersHighestScore()
		{
			int score = 0;
			for (int i = 0, j = _scores.Length; i < j; i++)
			{
				if (_scores[i].Score > score)
				{
					score = _scores[i].Score;
				}
			}
			return score;
		}
		#endregion

		#region private classes
		/// <summary>
		/// Stores a score for each player.
		/// </summary>
		[Serializable]
		private class PlayerScore
		{
			#region private attributes
			[SerializeField]
			private Scorer _scorer = null;
			[SerializeField]
			private IntReference _score = null;
			#endregion

			#region public properties
			public Scorer Scorer
			{
				get
				{
					return _scorer;
				}
			}

			public int Score
			{
				get
				{
					return _score.Value;
				}
				set
				{
					_score.Value = value;
				}
			}
			#endregion
		}
		#endregion
	}
}