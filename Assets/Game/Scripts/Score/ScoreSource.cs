﻿using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// An object that can give score to a player.
	/// </summary>
	public sealed class ScoreSource : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private IntReference _scoreValue = new IntReference(10);
		#endregion

		#region public functions
		/// <summary>
		/// Adds the score set on this object to the scorer.
		/// </summary>
		/// <param name="scorer"></param>
		public void AddScore(Scorer scorer)
		{
			ScoreBoard.AddScore(scorer, _scoreValue);
		}
		#endregion
	}
}