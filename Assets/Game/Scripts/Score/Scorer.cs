﻿using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// This class is used as a way of telling one player from the other when counting scores.
	/// It replaces the use of an enum, a good practice to avoid problems that could show up in the future.
	/// </summary>
	[CreateAssetMenu(fileName = CreateMenus.scorerFileName, menuName = CreateMenus.scorerMenu, order = CreateMenus.scorerOrder)]
	public sealed class Scorer : ScriptableObject
	{
	}
}