﻿using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Simplify the asembly of a player, by adding all the needed components.
	/// </summary>
	[RequireComponent(typeof(PlayerMovement))]
	[RequireComponent(typeof(PlayerShooting))]
	[RequireComponent(typeof(Hitable))]
	public sealed class Player : MonoBehaviour
	{
		#region private attributes
		private SpriteRenderer _renderer;
		private PlayerMovement _movement;
		private PlayerShooting _shooting;
		private Collider2D _collider;
		#endregion

		#region MonoBehaviour implementation
		private void Awake()
		{
			_renderer = GetComponent<SpriteRenderer>();
			_movement = GetComponent<PlayerMovement>();
			_shooting = GetComponent<PlayerShooting>();
			_collider = GetComponent<Collider2D>();
		}
		#endregion

		#region public functions
		/// <summary>
		/// Turn off the player components.
		/// </summary>
		public void TurnOff()
		{
			SetActive(false);
		}

		/// <summary>
		/// Turn on the player components.
		/// </summary>
		public void TurnOn()
		{
			SetActive(true);
		}
		#endregion

		#region private functions
		/// <summary>
		/// Turns the player components on or off.
		/// </summary>
		/// <param name="active"></param>
		private void SetActive(bool active)
		{
			_renderer.enabled = active;
			_movement.enabled = active;
			_shooting.enabled = active;
			_collider.enabled = active;
		}
		#endregion
	}
}