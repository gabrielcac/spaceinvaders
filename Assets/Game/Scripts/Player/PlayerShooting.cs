﻿using PandB.Variables;
using System.Collections;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Allows the player to shoot at the aliens.
	/// </summary>
	public sealed class PlayerShooting : MonoBehaviour
	{
		#region private attributes
		[Header("Input")]
		[SerializeField]
		private KeyCode _fire = KeyCode.Space;
		[Header("Configuration")]
		[SerializeField]
		private FloatReference _cooldown = new FloatReference(.5f);
		[Header("Events")]
		[SerializeField]
		private Vector3UnityEvent _onFire = null;

		private IEnumerator _coolDownCoroutine;
		#endregion

		#region MonoBehaviour implementation
		private void Update()
		{
			if(Input.GetKeyDown(_fire))
			{
				if(_coolDownCoroutine == null)
				{
					StartCoolDown();
					_onFire.Invoke(transform.position);
				}
				// Todo Add an else statement with a visual indication that the input worked but the canon is not ready to fire.
			}
		}

		private void OnDisable()
		{
			StopCoolDown();
		}
		#endregion

		#region private functions
		/// <summary>
		/// Starts the cooldown coroutine, making it impossible to shot again for a while.
		/// </summary>
		private void StartCoolDown()
		{
			StopCoolDown();
			_coolDownCoroutine = CoolDown(_cooldown);
			StartCoroutine(_coolDownCoroutine);
		}

		/// <summary>
		/// Stops the cooldown coroutine, and allow the player to shot again.
		/// </summary>
		private void StopCoolDown()
		{
			if(_coolDownCoroutine != null)
			{
				StopCoroutine(_coolDownCoroutine);
				_coolDownCoroutine = null;
			}
		}

		/// <summary>
		/// Cortoutine that monitors the time between each shot.
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		private IEnumerator CoolDown(float time)
		{
			yield return new WaitForSeconds(time);
			_coolDownCoroutine = null;
		}
		#endregion
	}
}