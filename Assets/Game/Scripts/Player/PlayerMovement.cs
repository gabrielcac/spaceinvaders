﻿using PandB;
using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// Moves the player character.
	/// </summary>
    public sealed class PlayerMovement : MonoBehaviour
    {
		#region private attributes
		[Header("Input")]
		[SerializeField]
		private KeyCode _moveLeft = KeyCode.A;
		[SerializeField]
		private KeyCode _moveRight = KeyCode.D;
		[Header("Movement Parameters")]
		[SerializeField]
		private FloatReference _speed = new FloatReference(270f);
		[SerializeField]
		[MinMaxRange(-270,270)]
		private MinMaxRange _movementLimits = new MinMaxRange();
		#endregion

		#region MonoBehaviour implementation
		private void Update()
		{
			if(Input.GetKey(_moveLeft))
			{
				Move(Vector3.left);
			}
			else if(Input.GetKey(_moveRight))
			{
				Move(Vector3.right);
			}
		}

		private void OnDrawGizmosSelected()
		{
			float yPos = transform.position.y;
			float zPos = transform.position.z;

			Gizmos.color = Color.red;
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeStart, yPos, zPos), new Vector3(_movementLimits.rangeEnd, yPos, zPos));
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeStart, yPos-5, zPos), new Vector3(_movementLimits.rangeStart, yPos+5, zPos));
			Gizmos.DrawLine(new Vector3(_movementLimits.rangeEnd, yPos-5, zPos), new Vector3(_movementLimits.rangeEnd, yPos+5, zPos));
		}
		#endregion

		#region public functions
		/// <summary>
		/// Moves the player back to the center of the screen.
		/// </summary>
		public void ResetPosition()
		{
			transform.localPosition = Vector3.zero;
		}
		#endregion

		#region private functions
		/// <summary>
		/// Moves the game object in the specified direction with the object speed.
		/// </summary>
		/// <param name="direction"></param>
		private void Move(Vector3 direction)
		{
			Vector3 newPosition = transform.position + _speed * Time.deltaTime * direction;
			if(newPosition.x < _movementLimits.rangeStart)
			{
				newPosition.x = _movementLimits.rangeStart;
			}
			else if(newPosition.x > _movementLimits.rangeEnd)
			{
				newPosition.x = _movementLimits.rangeEnd;
			}

			transform.position = newPosition;
		}
		#endregion
	}
}