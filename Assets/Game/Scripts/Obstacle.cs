﻿using PandB.Variables;
using UnityEngine;

namespace SpaceInvaders
{
	/// <summary>
	/// A barrier that stands between the players and the enemies and works as cover for both parties.
	/// </summary>
	public sealed class Obstacle : MonoBehaviour
	{
		#region private attributes
		[SerializeField]
		private IntReference _startingHitPoints = new IntReference(5);
		[SerializeField]
		private Animator _animator;
		[SerializeField]
		private StringReference _animatorLifeParameter = new StringReference("HpPercentage");

		private int _hitPoints;
		#endregion

		#region MonoBehaviour implementation
		private void Reset()
		{
			if(_animator == null)
			{
				_animator = GetComponent<Animator>();
			}
		}

		private void Awake()
		{
			_hitPoints = _startingHitPoints;
		}
		#endregion

		#region public functions
		/// <summary>
		/// Takes a hit point from the obstacle total and updates the animator.
		/// </summary>
		public void Hit()
		{
			_hitPoints -= 1;
			_animator.SetFloat(_animatorLifeParameter, (float)_hitPoints / _startingHitPoints.Value);
		}
		#endregion
	}
}